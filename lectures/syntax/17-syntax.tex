\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[margin=.3in]{geometry} 
\usepackage{../201-style}
\usepackage{mathtools}
\usepackage{setspace}
\setitemize{itemsep=1pt,topsep=1pt,parsep=1pt,partopsep=1pt}
\setenumerate{itemsep=1pt,topsep=1pt,parsep=1pt,partopsep=1pt}
\usepackage{fancyhdr}
\pagestyle{fancy}
\renewcommand\headrulewidth{0pt}
\lhead{}\chead{}\rhead{}
\rfoot{\vspace*{-4em}\thepage}
\newcommand{\ruleme}{\underline{\hspace{\linewidth}}}


\newcommand{\gap}{\underline{\phantom{xx}}}
\begin{document}
\setlength{\parindent}{0pt}
{\Large\bld{Linguistics 201 \quad Introduction to linguistic theory (spring 2019)}}\vspace{.25em}

{\large Day 19 \tdot\  Syntax (April 9th, 2019) \hfill Deniz \"Ozy\i{}ld\i{}z  \quad \texttt{deniz.fr/201/} 
}\vspace{.75em}

\begin{itemize}
	\item Last time, we saw how to build up Sentences (S), Verb Phrases (VPs), Noun Phrases (NPs), and Prepositional Phrases (PPs). Let's expand our inventory.
		\begin{itemize} 
			\item \bld{Complementizer Phrases} (CP)

				Some verbs and nouns combine with things that look like sentences:
				\ex. \a. Dominique said that Nico solved the problem.\label{ex:cp1}
				\b. That Nico solved the problem delighted Dominique.
				\b. The rumor that Nico solved the problem delighted Dominique.
				\z.

				\begin{itemize}
					\item These things often come with a complementizer: `that, if, whether.'
					\item And they contain a sentence.
					\item We will call these things Complementizer Phrases (CPs) and create them with the following rule:
						\begin{center}
							CP \ra\ C S
						\end{center}

						\begin{multicols}{2}
							\begin{center}
								Tree for the CP in \ref{ex:cp1}

								\begin{forest}
									[ CP 
										[ C [ that ] ]
										[ S 
											[ NP [ N [ Nico ] ] ]
											[ VP 
												[ V [ solved ] ]
												[ NP 
													[ D [ the ] ]
													[ N [ problem ] ]
												]
											]
										] 
									]
								\end{forest}
							\end{center}
							\columnbreak
							{\color{white}x}
						\end{multicols}
				\end{itemize}

			\item \bld{Coordination} 
				\ex. \a. Paloma and a young student solved the problem.\hfill NP \ra\ NP and NP\label{ex:c1}
				\b. Paloma smiled and solved the problem.\hfill VP \ra\ VP and VP
				\b. Paloma climbed down a fence and up a tree.\hfill PP \ra\ PP and PP
				\b. Paloma arrived and the cat purred.\hfill S \ra\ S and S
				\z.\label{ex:c}

				\begin{itemize}
					\item Using words like `and' to join phrases together is called coordination.
					\item Observations:

						\#1 `And' wants one thing to its left and one thing to its right.

						\#2 `And' is not picky as to the category of those things: NPs, VPs, PPs and Ss can all be coordinated.

						\#3 The category of the entire coordination is the same as the category of the two coordinated things.
				\end{itemize} 
			\item We can account for all of these observations with the following rule, where $\alpha$ can be any of the categories that we've seen.

				\begin{center}
					$\alpha$ \ra\ $\alpha$ and $\alpha$
				\end{center}

				\begin{multicols}{2}
					\begin{center}
						Tree for the NP in \ref{ex:c1}

						\begin{forest}
							[ NP 
								[ NP [ N [ Paloma ] ] ]
								[ and ]
								[ NP 
									[ D [ a ] ] 
									[ A [ young ] ]
									[ N [ student ] ]
								]
							]
						\end{forest}
					\end{center}\columnbreak

					\begin{center}
						Draw one of the other trees for \ref{ex:c}

					\end{center}
				\end{multicols} 
		\end{itemize}
	\item \bld{Recursion} 

		\begin{itemize}
			\item There are \bld{infinitely many} sentences of English.\footnote{A vast majority of linguists think that this is one of the defining property of human language.}

				For example, in \Next, we see that you can add as many PPs as you like to a VP, and you'll get a sentence.
				\ex. \a. Paloma walked.
				\b. Paloma walked into the room.
				\b. Paloma walked into the room past the buffet.
				\b. Paloma walked into the room past the buffet through the curtain.
				\b.[] \ldots
				\z.

			\item Our rules don't yet capture this fact.
				\begin{itemize} 
					\item The rule VP \ra\ V (NP) (PP) says that there can be up to one PP in a VP.
					\item We can add more rules: VP \ra\ V (NP) (PP) (PP) says that there can be up to two PPs in a VP.
					\item But this won't capture the observation that there can be infinitely many PPs in a VP.
				\end{itemize}
			\item To fix that, we will make some of our rules recursive.  

				Recursion is when an expression of some category contains another expression of the same category.

				\begin{itemize}
					\item \bld{VP} \ra\ \bld{VP} PP
				\end{itemize}
			\item And change our other rules accordingly.
				\begin{itemize}
					\item VP \ra\ V (NP)
				\end{itemize}
				\begin{multicols}{2}

					Using the VP \ra\ VP PP rule once

					\begin{forest}
						[ VP 
							[ VP [ V [ walked ] ] ]
							[ PP 
								[ P [ into ] ] 
								[ NP 
									[ D [ the ] ]
									[ N [ room ] ]
								]
							]
						]
					\end{forest}\columnbreak

					Using the VP \ra\ VP PP rule twice

					\begin{forest}
						[ VP 
							[ VP 
								[ VP [ V [ walked ] ] ]
								[ PP 
									[ P [ into ] ] 
									[ NP 
										[ D [ the ] ]
										[ NP [ N [ room ] ] ]
									]
								]
							]
							[ PP 
								[ P [ past ] ]
								[ NP 
									[ D [ the ] ] 
									[ NP [ N [ buffet ] ] ] 
								]
							]
						]
					\end{forest}
				\end{multicols}
			\item Noun phrases can also contain other noun phrases.
				\ex. \a. The student slept.
				\b. The student with the hat slept.
				\b. The student with the hat behind Paloma slept.
				\b. The student with the hat behind Paloma in the third row slept.
				%\b.[]\ldots
				\z.

				\begin{multicols}{2}
			\item So, we can add the following recursive rule: 
				\begin{itemize}
					\item \bld{NP} \ra\ \bld{NP} (PP)
				\end{itemize}

			\item And change our previous NP rule accordingly:
				\begin{itemize}
					\item NP \ra\ (D) (A) N
				\end{itemize}
				\end{multicols}
		\end{itemize}

		\begin{multicols}{2}
			Applying the NP \ra\ NP (PP) rule once

			\begin{forest}
				[ S
					[ NP 
						[ NP 
							[ D [ the ] ]
							[ N [ student ] ]
						]
						[ PP 
							[ P [ with ] ]
							[ NP 
								[ D [ the ] ]
								[ N [ hat ] ]
							]
						]
					]
					[ VP [ V [ slept ] ] ]
				]
			\end{forest}
			\columnbreak


			Applying the NP \ra\ NP (PP) rule twice

			\begin{forest}
				[ S
					[ NP 
						[ NP
							[ NP 
								[ D [ the ] ]
								[ N [ student ] ]
							]
							[ PP 
								[ P [ with ] ]
								[ NP 
									[ D [ the ] ]
									[ N [ hat ] ]
								]
							]
						]
						[ PP
							[ P [ behind ] ]
							[ NP [ N [ Paloma ] ] ]
						]
					]
					[ VP [ V [ slept ] ] ]
				]
			\end{forest}
		\end{multicols} 
	
	\vspace{-2em}
	\item \bld{Structural ambiguity}: Some sentences are ambiguous.
		\ex. Paloma saw the man with the telescope.
		\a. Meaning \#1: Paloma used a telescope to see the man. The man did not have a telescope.
		\b. Meaning \#2: The man had a telescope. Paloma did not use a telescope to see the man.  
		
		\begin{itemize}
			\item The reason behind this ambiguity is that there are two structures associated with this sentence.

				\begin{itemize}
					\item One where the PP `with the telescope' combines with the VP `saw the man.'
					\item Another where the PP `with the telescope' combines with the NP `the man.'
				\end{itemize}

				\begin{multicols}{2}
					Tree corresponding to Meaning \#1

					\begin{forest}
						[ S
							[ NP [ N [ Paloma ] ] ]
							[ VP 
								[ VP 
									[ V [ saw ] ] 
									[ NP 
										[ D [ the ] ]
										[ N [ man ] ]
									]
								]
								[ PP 
									[ P [ with ] ]
									[ NP 
										[ D [ the ] ]
										[ N [ telescope ] ]
									]
								]
							]
						] 
					\end{forest}
					\columnbreak 
					
					Tree corresponding to Meaning \#1
				\end{multicols} 
		\end{itemize}

	\item Structural ambiguity in the wild:

		\begin{multicols}{2}
			\ex. \a. British Left Waffles on Falkland Islands
			\b. Dr. Ruth to Talk about Sex with Newspaper Editors
			\b. Squad Helps Dog Bite Victim
			\b. Grandmother of Eight Makes Hole in One
			\z.

		\end{multicols} 
	
	\item \bld{Constituency} 

		There are two ways of thinking about sentences.

		\ex. Paloma saw the elegant hamster.

		\begin{itemize}
			\item As a `flat' sequence of words or sounds: \tpb{p@lowm@sa\dh @m\ae nwIT\dh @h\ae mpst@r}.
			\item As things that have structure, represented by the trees that we have been drawing.
		\end{itemize}

		These tree structures contain some information that isn't present in flat sequences of words.

		\begin{multicols}{2}
			\begin{forest}
				[ S 
					[ NP [ N [ Paloma ] ] ]
					[ VP 
						[ V [ saw ] ]
						[ NP
							[ D [ the ] ]
							[ A [ elegant ] ]
							[ N [ hamster ] ]
						]
					]
				] 
			\end{forest}

			\begin{itemize}
				\item The words `the' `elegant' \& `hamster' form a unit.
				\item The words `the' and `elegant' don't form a unit.
				\item The words `saw' and `the' don't form a unit.  
				\item Other units:
					\begin{itemize}
						\item The VP `saw the elegant hamster'
						\item The S `Paloma saw the elegant hamster'
					\end{itemize} 
				\item Other things that aren't units:
					\begin{itemize}
						\item `Paloma saw'
						\item `Paloma saw the'
						\item `Paloma saw the elegant'
					\end{itemize}
			\end{itemize}

		\end{multicols}

		\begin{itemize}
			\item These units are called \bld{constituents}.
			\item Thinking of sentences as sequences of words gives us no way of capturing the fact that there are constituents.
			\item We also have no way of accounting for the ambiguity associated with sentences like 
				\ex. Paloma saw the man with the telescope.
				\a. Meaning \#1: The seeing happened with the telescope.\\
				The VP `saw the man' and the PP `with the telescope' form a constituent. The NP `the man' and the PP don't.
				\b. Meaning \#2: The man had a telescope.\\
				The NP `the man' and the PP `with the telescope' form a constituent. The VP `saw the man' and the PP don't.

		\end{itemize} 
	\item How to detect constituents? [More on this next time.]
		\begin{itemize}
			\item Coordination

				\ex. \a. Paloma saw the elegant hamster and the man.
				\b. Paloma saw the elegant hamster and laughed.
				\b. *Paloma saw the and liked the elegant hamster.
				\z.

			\item Substitution
				\ex. \a. Paloma saw the elegant hamster. Mary saw it too.
				\b. Paloma saw the elegant hamster. Mary did too. 

		\end{itemize}
	\item Fun fact: ``Paloma saw the man with the telescope and Chloe did with binoculars'' is not ambiguous. Why?
\end{itemize} 
\end{document}
