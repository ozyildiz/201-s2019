\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[margin=.3in]{geometry} 
\usepackage{../201-style}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{setspace}
\setitemize{itemsep=1pt,topsep=1pt,parsep=1pt,partopsep=1pt}
\setenumerate{itemsep=1pt,topsep=1pt,parsep=1pt,partopsep=1pt}
\usepackage{fancyhdr}
\pagestyle{fancy}
\renewcommand\headrulewidth{0pt}
\lhead{}\chead{}\rhead{}
\rfoot{\vspace*{-4em}\thepage}
\newcommand{\ruleme}{\underline{\hspace{\linewidth}}}


\newcommand{\gap}{\underline{\phantom{xx}}}
\begin{document}
\setlength{\parindent}{0pt}
{\Large\bld{Linguistics 201 \quad Introduction to linguistic theory (spring 2019)}}\vspace{.25em}

{\large Day 20 \tdot\  Syntax (April 11th, 2019) \hfill Deniz \"Ozy\i{}ld\i{}z  \quad \texttt{deniz.fr/201/} 
}\vspace{.75em}

\begin{itemize}
	\item \bld{An open end from last time}

		We saw that the following sentence, and sentences like it, were ambiguous:
		\ex. Paloma saw the man with the telescope.
		\a. Either: The seeing happened with the telescope.
		\b. Or: The man had a telescope.
		\z.

		This is because the PP `with the telescope' can either attach to the VP `saw the man' or to the NP `the man.'

		\begin{itemize}
			\item Because there are two possible things that the PP can attach to, we should be able to attach it to both.

				\ex. Paloma saw the man with the telescope with the telescope.\\
				Both: The seeing happened with the telescope and the man had a telescope.

			\item There are ways of disambiguating \LLast.

				\begin{itemize}
					\item In English, it is possible to swap a VP with `did (too)'

						\ex. Paloma laughed\ldots\ and Masha did too.

						This means that we should be able to replace the VP in our ambiguous sentence with `did' as well:

						\ex. Masha saw the man with the binoculars\ldots\ and Paloma did with the telescope.\\
						The seeing happened with the telescope.\hfill (did=see the man)

						But doing this erases `the man,' so that the PP can no longer attach to it.

						\begin{center}
							\begin{forest}
								[ S 
									[ NP [ N [ Paloma ] ] ]
									[ VP 
										[ V [ {did\\{\color{gray}(=see the man)}} ] ] 
										[ PP 
											[ P [ with ] ]
											[ NP 
												[ D [ the ] ]
												[ N [ telescope ] ]
											]
										]
									]
								]
							\end{forest}
						\end{center}


					\item In English, it is possible to move things around in a sentence. Consider:

						\ex. The man, Paloma saw.

						Doing this kind of thing to our ambiguous sentence will also make the ambiguity disappear:

						\ex. \a. The man with the telescope, Paloma saw.\\
						The man had a telescope.
						\b. The man, Paloma saw with the telescope.\\
						The seeing happened with the telescope.

					\item Take home: You can carefully do things to sentences to figure out what structure(s) they have.
					%\item In English, it is also possible to replace NPs with pronouns, like `him.' These pronouns often don't like to be combined with PPs.  
					%	\ex. Paloma saw him with binoculars.\\
					%	The seeing happened with the telescope.

				\end{itemize} 
		\end{itemize}


	\item \bld{Recursion} 
		\begin{itemize}
			\item We've seen that NPs and VPs can contain infintely many PPs.  
				\ex. \a. Paloma walked into the room past the buffet through the window.
				\b. The student by Paloma with the hat in the third row slept.
				\z.

			\item We've amended our NP and VP rules so that this observation could be accounted for.
				\begin{multicols}{2}
					\ex. \a. \bld{NP} \ra\ \bld{NP} PP
					\b. NP \ra\ (D) (A) N
					\b. \bld{VP} \ra\ \bld{VP} PP
					\b. VP \ra\ V (NP)

				\end{multicols}

				\begin{multicols}{2}
					\ex. Our other rules
					\a. S \ra\ NP VP
					\b. P \ra\ P (NP)\columnbreak
					\b.[]
					\b. CP \ra\ C S
					\b. $\alpha$ \ra\ $\alpha$ and $\alpha$

				\end{multicols}


				The fact that a rule has the same category to the left and to the right of the \ra\ makes it so that the rule can be applied infinitely many times, creating infinitely many phrases of English.

			\item We've seen a similar thing for the coordination rule $\alpha \rightarrow \alpha\ \text{and}\ \alpha$, and you should uncover a similar thing for CPs in assignment \#4.

			\item The thing is is that \bld{adjectives can be repeated as well}.

				\ex. \a. The big elephant laughed.
				\b. The big tall elephant laughed.
				\b. The big tall lazy elephant laughed.
				\b. The big tall lazy smelly elephant laughed.
				\b. The big tall lazy smelly shy elephant laughed.
				%\b. The big tall lazy smelly shy incautious elephant laughed.
				\z.

				And we would like to account for this observation.

			\item One might be tempted to swap our old NP rules for new ones, like so\ldots 
				\ex. \a. old NP rules
				\a. NP \ra\ (D) (A) N
				\b. NP \ra\ NP PP
				\z.
				\b. (hypothetical) new NP rules
				\a. NP \ra\ (D) (A) NP
				% \b. NP \ra\ A NP
				\b. NP \ra\ NP PP
				\z.

				This would allow us to capture the fact that there may be as many adjectives as you want within an NP.

				\begin{center}
					\small 
					\begin{forest}
						[ NP 
							[ D [ the ] ]
							[ NP 
								[ A [ big ] ]
								[ NP 
									[ A [ tall ] ]
									[ NP 
										[ N [ elephant ] ]
									]
								]
							]
						]
					\end{forest}
				\end{center}

			\item But\ldots\ there's a problem. Can you detect it?
			\item With this new set of rules, we predict that things like the following should be OK!
				\ex. \a. *The big the elephant laughed.
				\b. \begin{forest}
					[ S
						[ NP 
							[ D [ the ] ]
							[ NP
								[ A [ big ] ]
								[ NP 
									[ D [ the ] ] 
									[ NP [ elephant ] ]
								]
							]
						]
						[ VP [ V [ laughed ] ] ]
					]
				\end{forest}

			\item There are languages that are able to repeat the determiner (and this is, to my knowledge, not well understood).

				\exg. i orea i xoreftria\\
				the beautiful the dancer \\
				\glt the beautiful dancer\hfill [Greek]

			\item But English is not one such language and we need to block what's going on in \LLast.
			\item One way of doing so is to create a new syntactic category that can only exist within the NP. 
				
				Call this \nbar. Read ``N bar,'' because there's a bar on top of the N.

				\ex. \a. NP \ra\ (D) \nbar
				\b. \nbar\ \ra\ A \nbar
				\b. \nbar\ \ra\ N
				\z. 


			\item Now, we can have as many adjectives as we want, without worrying about there being too many determiners.

				(There is no way now to have more than one determiner.)

				\begin{center}
					\begin{forest}
						[ NP 
							[ D [ the ] ]
							[ \nbar{}
								[ A [ big ] ]
								[ \nbar\ 
									[ A [ tall ] ] 
									[ N [ \nbar\ [ elephant ] ] ] 
								]
							]
					] 
					\end{forest}
				\end{center}

			\item Notice that we've left out PPs. How would you find a way of incorporating PPs into the mix? In other words, how would we now create sentences like ``The big tall elephant in the third row laughed''? 

				% Note that NP \ra\ NP PP won't do anymore, because it predicts that things like `[ [ the dog ] on the basket ]' to be good.
		\end{itemize}

		\clearpage
	\item \bld{Constituents}


		\ex. \a. Alice saw the dog.
		\b. \begin{forest}
			[ S
				[ NP [ N [ Alice ] ] ]
				[ VP
					[ V [ saw ] ]
					[ NP 
						[ D [ the ] ]
						[ N [ dog ] ]
					]
				]
			]
		\end{forest}

		\begin{itemize}
			\item This tree representation says that `the dog' and `saw the dog' form units, and that things like `saw the' don't. Is there other evidence for this?
			\item \bld{Substitution}
				\begin{itemize}
					\item \bld{Pronoun substitution}

						\ex. Alice saw the dog. Mary saw \bld{it} too.\hfill [it=the dog]

					\item \bld{Proverb substitution}

						\ex. Alice saw the dog. Mary \bld{did} too.\hfill [did=saw the dog]

					\item \bld{Prosentence substitution}
						
						\ex. Clara thinks that Alice saw the dog. Mary thinks \bld{so} too.\hfill [so=that Alice saw the dog]

					\item[$\Rightarrow$] We can replace certain \emph{nodes} in a tree by a single word. This suggests that the nodes that we can replace by a single word form units.

					\item Notice that we can't replace things like `saw the' by a single word. This suggests that `saw the' does not form a unit.

				\end{itemize}
			\item \bld{Coordination}
				\begin{itemize}
					\item \bld{NP coordination}
						\ex. Alice saw the dog and the black cat.

					\item \bld{VP coordination}
						\ex. Alice saw the dog and laughed.

					\item \bld{S coordination}
						\ex. Alice saw the dog and Mary laughed.

					\item[$\Rightarrow$] We can replace certain nodes in a tree by a coordination of two things of the same syntactic type.
						
					\item We cannot coordinate things that do not form a unit.

						\ex. *Alice saw a and kissed a cat.

				\end{itemize} 
				\clearpage
			\item \bld{Movement}
				\begin{itemize}
					\item \bld{NP movement}\hfill [Yoda speak]
						\ex. The dog, Alice saw.

					\item \bld{VP movement}
						\ex. See the dog, Alice did.

					\item \bld{S movement}
						\ex. That Alice saw the dog, Mary thinks.

					\item[$\Rightarrow$] Usually, you can't move things around that do not form constituents.

						\ex. *Saw the Alice dog.

				\end{itemize}

		\end{itemize}

	\end{itemize}
\end{document}
