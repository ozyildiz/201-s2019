\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[margin=.3in]{geometry} 
\usepackage{../201-style}
\usepackage{mathtools}
\usepackage{setspace}
\setitemize{itemsep=1pt,topsep=1pt,parsep=1pt,partopsep=1pt}
\setenumerate{itemsep=1pt,topsep=1pt,parsep=1pt,partopsep=1pt}
\usepackage{fancyhdr}
\pagestyle{fancy}
\renewcommand\headrulewidth{0pt}
\lhead{}\chead{}\rhead{}
\rfoot{\vspace*{-4em}\thepage}
\newcommand{\ruleme}{\underline{\hspace{\linewidth}}}

\newcommand{\ra}{$\rightarrow$\ }

\newcommand{\gap}{\underline{\phantom{xx}}}
\begin{document}
\setlength{\parindent}{0pt}
{\Large\bld{Linguistics 201 \quad Introduction to linguistic theory (spring 2019)}}\vspace{.25em}

{\large Day 17 \tdot\  Introducing Syntax (April 2nd, 2019) \hfill Deniz \"Ozy\i{}ld\i{}z  \quad \texttt{deniz.fr/201/} 
}\vspace{.75em}

\begin{itemize}
	\item Morphology: The building blocks and the rules behind words \& compounds. 
	\item \bld{Introducing syntax} 

		Our knowledge of a language involves knowing which sentences are possible, and which are impossible.

		\begin{multicols}{3}
			\ex. Emily drives a big car.

			\columnbreak

			\ex. \a. *Emily a big car drives.
			\b. *Drives Emily a big car.
			\z.

			\columnbreak

			\hfill \small $\leftarrow$ `*' means ungrammatical

		\end{multicols}

		This knowledge extends to things that are smaller than sentences, yet bigger than words \& compounds.\footnote{IRL, the line between morphology and syntax is hard to draw.}

		\begin{multicols}{3}
			\ex. \a. Emily drives a big car.
			\b. *Emily drives a car big.
			\z.

			\ex. \a. Emily's car is big.
			\b. *Car Emily's is big.
			\z.

			\ex. \a. Emily drives for fun.
			\b. *Emily drives fun for.
			\z.

		\end{multicols}

	\item[$\Rightarrow$] \bld{Syntax} investigates the structure of of sentences and linguistic expressions that are bigger than words.
		\begin{itemize}
			\item Sentences that are possible in a language are called \bld{grammatical}, or \bld{well-formed}.
			\item Sentences that are \emph{im}possible in a language are called \bld{ungrammatical}, or \bld{ill-formed}.  
		\end{itemize}

	\item The reason that some of these sentences are grammatical and others are not is that some follow the \bld{syntactic rules} of English, and others don't.
		\begin{itemize}
			\item In syntax, these rules refer to the \bld{syntactic category} of words ($\approx$ their part of speech).  

				\begin{itemize}
					\item Good syntactic rules and bad syntactic rules Part \#1
						\begin{itemize} 
							\item Not a good syntactic rule:\\
								A sentence can be formed from the words `Emily' and `drives.'
							\item A good syntactic rule:\\
								A sentence can be formed from a \bld{noun} and a \bld{verb}. (This rule is \emph{general}.) 
								% It applies to \emph{all} nouns and \emph{many} verbs.)
						\end{itemize}
					\item Some syntactic categories:
						\begin{itemize}
							\item A(djective): big, bad, long, happy, careful\ldots 
							\item N(oun): Emily, car, fun\ldots 
							\item V(erb): drive, walk, consider, know, say\ldots
							\item P(reposition): for, to, from, on, behind, after, since, of\ldots
							\item D(eterminer): the, that, a, some, this, every\ldots
						\end{itemize}
					\item Evidence for this: 
						\begin{itemize}
							\item There is nothing ungrammatical about the following, which has many words that aren't really words.
								\ex. `Twas brillig, and the slithy toves\\
								Did gyre and gimble in the wabe:\\
								All mimsy were the borogoves,\\
								And the mome raths outgrabe.\quad\quad\quad Lewis Carroll

							\item The following, on the other hand, really sounds awful, despite the fact that it also contains non-words.
								\ex. *All were the borogoves mimsy.

							\item Moreover, we use our knowledge of the rules of English to figure out that some of these non-words are adjectives (`brillig, slithy, mimsy'), some are nouns (`toves, wabe, borogoves'), and some are verbs (`gyre, gimble, outgrabe').
							\item And, we can use the same knowledge to construct new sentences out of these non-words.
								\ex. A brillig tove mimsily gyred with the borogove.

						\end{itemize}
				\end{itemize}
			\item How do we tell whether a sentence is grammatical or ungrammatical?

				There's only one way of making sure: \bld{Ask a native speaker}.\\[-1em]

				\bld{Practice}:\\
				Which one of these English sentences is ungrammatical?
				\begin{multicols}{2}
					\ex. \a. Molly ate the apple.
						\b. Molly devoured the apple.\columnbreak
						\b. Molly ate.
						\b. Molly devoured.\columnbreak
						\z.

				\end{multicols}

				Which one of these Turkish sentences is ungrammatical?
				\begin{multicols}{2}
					\ex. \ag. Emily b\"uy\"uk bir araba kullan\i{}yor.\\
					Emily big a car drives\\
					\glt {\color{white}x}
					\bg. B\"uy\"uk Emily bir araba kullan\i{}yor.\\
					big Emily a car drives\\
					\glt {\color{white}x}
					\bg. Emily bir araba kullan\i{}yor b\"uy\"uk.\\
					Emily a car drives big\\
					\glt {\color{white}x}

				\end{multicols}

				The reason for asking a native speaker is that acquiring a first language shapes the brain in a very special way, and gives us the ability to \emph{judge} any sentence in that language as grammatical or ungrammatical.

			\item Good syntactic rules and bad syntactic rules Part \#2: \bld{Prescriptive} vs.\ \bld{descriptive} rules.
				
				Which these sentences are ungrammatical?

				\begin{multicols}{2}
					\ex. \a. Who did Molly see?
					\b. Whom did Molly see?

					\ex. \a. Who did he run away with?
					\b. With who did he run away?

				\end{multicols}

				It turns out that people other than linguists are interested in the rules of language.

				These people might say things like:
				\begin{itemize}
					\item You \emph{should} say `whom' and not `who' if `who' is the object of a verb!
					\item Prepositions are \emph{not acceptable} things to end your sentences with!
				\end{itemize} 

				But\ldots\ this isn't how we really speak. (Perhaps it is how some people write.) In fact, we do the exact opposite.

				Rules like this are called \bld{prescriptive}. They tell you what you should do. (Think `prescription.')

				Linguists are interested in describing how people really speak. 
				
				The rules that we're interested in are \bld{descriptive}. They describe the language, and don't boss you around.

				Here are some of the kinds of rules that linguists are interested in:
				\begin{itemize}
					\item In English, a sentence can be made up of noun and a verb in thas order.
						\ex. Emily drives. \\ Molly ate. \\ Anastasia paints.

					\item In Samoan, a sentence can be made up of a verb and a noun in that order.

						\exg. \tp{Puao:} \tp{ta\ng ata} \\
						left the~people \\
						\glt The people left.\hfill {\small (Modified from Alderete \& Bradshaw 2012)} 

				\end{itemize} 
		\end{itemize}

		\begin{mdframed}
			\begin{center} 
				Our job, as budding syntacticians, will be to reverse-engineer these rules.
			\end{center}
		\end{mdframed}

		\clearpage
	\item \bld{Why do we look at ungrammatical sentences?} 
		
		\ex. \bld{Thought experiment}: \\
		Suppose Emily is watching soccer games and has to figure out the offside rule on her own.\footnote{This rule says that while the ball is being played by your team, you can't be closer to your opponent's goal than the closest defender. \\ This is so that you don't camp by the goal waiting for someone to pass you the ball. [Disclaimer: I know next to nothing about soccer.]}\\[.25em]
		What has to happen for her to be able to figure out the rule?
		\a. No player ever breaks the rule.
		\b. The rule is broken several times.
		\z. 

		
		\sem{\Rightarrow} We are just like Emily\ldots\ we need the rules to be broken to be able to figure out what they are.
		
	\item Now let's start figuring out the actual rules!

		\begin{itemize}
			\item You can form a sentence of English by combining a N and a V.  
				
				\begin{multicols}{3}
					\ex. \a. Emily drives.
						\b. People laugh.
						\b. Ice melts.

				\end{multicols}

			\item There is a formal notation for this rule.
				\ex. S \ra\ N V

			\item And we can use these rules to make tree structures. 
				\begin{multicols}{3}
					\ex. \a. 
					\begin{forest}
						[ S
							[ N [ Emily ] ]
							[ V [ drives ] ]
						]
					\end{forest} 
					\b.
					\begin{forest}
						[ S
							[ N [ People ] ]
							[ V [ laugh ] ]
						]
					\end{forest}
					\b.
					\begin{forest}
						[ S
							[ N [ Ice ] ]
							[ V [ melts ] ]
						]
					\end{forest}

				\end{multicols} 
		\end{itemize}

	\item \bld{Noun phrases} 
		\begin{itemize}
			\item Many sentences of English can't be formed by the `S \ra\ N V' rule.
				\begin{multicols}{2}
					\ex. \a. People laugh.
					\b. Some people laugh.
					\b. Sneaky people laugh.
					\b. Some sneaky people laugh.

					S \ra\ \bld{N} V

					S \ra\ \bld{D N} V

					S \ra\ \bld{A N} V

					S \ra\ \bld{D A N} V
				\end{multicols}

			\item These rules correspond to tree structures, just like before.

				\begin{multicols}{3}
					\begin{forest}
						[ S 
							[ D [ some ] ]
							[ N [ people ] ]
							[ V [ laugh ] ]
						]
					\end{forest} 
					\begin{forest}
						[ S 
							[ A [ sneaky ] ]
							[ N [ people ] ]
							[ V [ laugh ] ]
						]
					\end{forest} 
					\begin{forest}
						[ S 
							[ D [ some ] ]
							[ A [ sneaky ] ]
							[ N [ people ] ]
							[ V [ laugh ] ]
						]
					\end{forest}
				\end{multicols}
			\item These rules look like they're getting tedious.\\
				They're also missing a pattern: Wherever you can use an N, you can also use D N, A N, and D A N.\\
				In a sense, these things are `the same kind of thing.'

				\clearpage
			\item Let's call this thing a \bld{Noun Phrase} (NP). Noun phrases include:
				\begin{multicols}{2}
					\ex. \a. NP \ra\ N \hfill `people' 
					\b. NP \ra\ D N \hfill `some people'
					\b. NP \ra\ A N \hfill `sneaky people'
					\b. NP \ra\ D A N \hfill `some sneaky people'

				\end{multicols}

			\item With these new rules, our tree structures will also look different.

				But first, we need to make a change to our `S \ra\ N V' rule.

				What is the change that we need to make? \quad\underline{\hspace{15em}}

				\begin{multicols}{3}
					\begin{forest}
						[ S 
							[ NP 
								[ D [ some ] ]
								[ N [ people ] ]
							]
							[ V [ laugh ] ]
						]
					\end{forest} 

					\begin{forest}
						[ S 
							[ NP 
								[ A [ sneaky ] ]
								[ N [ people ] ]
							]
							[ V [ laugh ] ]
						]
					\end{forest} 

					\begin{forest}
						[ S 
							[ NP 
								[ D [ some ] ]
								[ A [ sneaky ] ]
								[ N [ people ] ]
							]
							[ V [ laugh ] ]
						]
					\end{forest} 
				\end{multicols}

			\item But wait\ldots\ haven't we just complicated things? We still need 5 rules \textsc{plus} we need a new concept, the NP.

			\item With this new concept in our quiver, we can tackle many many new cases without needing that many rules.

				Indeed, sentences can get syntactically very complicated very quickly:
				\begin{multicols}{2}
					\ex. \a. People kick buckets.
					\b. People kick some buckets.
					\b. People kick sneaky buckets.
					\b. People kick some sneaky buckets.

					\columnbreak

					\ex. \a. Some people kick some buckets.
					\b. {Sneaky people kick sneaky buckets.}
					\b. {\footnotesize Some sneaky people kick \hbox{some sneaky buckets.}}
					\b.[] \ldots
					\z.

				\end{multicols}

			\item Instead of writing 7 new and different rules like this:

				\begin{multicols}{3}
					\ex. \a. S \ra\ N V N
					\b. S \ra\ N V D N 
					\b. S \ra\ N V A N 
					\b. S \ra\ N V D A N 
					\b. S \ra\ D N V D N
					\b. S \ra\ A N V A N
					\b. S \ra\ D A N V D \hbox{A N}

				\end{multicols} 

			\item We simply need a single rule: S \ra\ NP V NP. This will give us trees like:

				\begin{forest}
					[ S
						[ NP 
							[ D [ some ] ]
							[ N [ people ] ]
						]
						[ V [ kick ] ]
						[ NP 
							[ A [ sneaky ] ]
							[ N [ buckets ] ]
						]
					]
				\end{forest} 

			\item A summary of the rules that we've seen so far:\\

				\begin{tabular}{ll}
					\toprule
					rule & example\\
					\midrule 
					NP \ra\ N & people \\
					NP \ra\ D N & some people \\
					NP \ra\ A N & sneaky people \\
					NP \ra\ D A N & some sneaky buckets \\\addlinespace[.5em]
					S \ra\ NP V & Sneaky people laugh.\\ 
					S \ra\ NP V NP & Sneaky people kick some sneaky buckets.\\
					\bottomrule
				\end{tabular}
			\hfill\bld{to be continued\ldots}
		\end{itemize} 

\end{itemize}
		
\end{document}
		Suppose no violation of that rule occurs. Can she make any guesses as to what that rule says?

		No.

		Suppose now that she observes Zinedine break the rule during the first half of the game. 

		\begin{itemize}
			\item If Zinedine is in some position
			\item If the players are in some position during the first half of the game, offside
		\end{itemize}

		So she must learn to factor out the things that are irrelevant to the rule. Which player is offside doesn't matter. The time at which a player is offside doesn't matter. It doesn't matter whether it was raining when the player was offside, etc.

		In some cases, we know what won't matter. Swapping names, for instance, will never make an ungrammatical sentence.

		\ex. \a. John runs.
		\b. Mary runs.
		\b. Emily runs.
		\z.

		In others, we might not. So we test hypotheses: ``This will break the rule.''

		Crucially though---\bld{violations of linguistic rules must be observed} for a linguist to be able to reverse-engineer.

		This is why we will be looking at ungrammatical sentences.

	\item How do we know whether a linguistic expression is grammatical or not? We take sentences, and have native speakers `judge' them. 
		
		This means that they tell the linguist ``Yes, this is a good sentence of my language,'' ``Nope, this is not a sentence of my language,'' or ``This sentence sounds better than this other sentence.''

		There are other ways of formulating judgments: ``Makes sense,'' etc.

		Three main kinds of judgments 

		\ex. \a. *Emily the rule figured out.
		\b. Emily figured out anything.
		\b. Context: Emily didn't figure out the rule.\\
		Emily figured out the rule. [False]
		\z.

	\item Independence of syntax 

		We're able to judge sentences as good or bad partly on the basis of parts of speech, or grammatical category.

		\ex. \a. Colorless green ideas sleep furiously.
		\b. `Twas brillig, and the slithy toves\\
		Did gyre and gimble in the wabe:\\
		All mimsy were the borogoves,\\
		And the mome raths outgrabe.\\[\baselineskip]
		``Beware the Jabberwock, my son!\\
		The jaws that bite, the claws that catch!\\
		Beware the Jubjub bird, and shun\\
		The frumious Bandersnatch!''

		Function words!

	\item Rules



\end{itemize}
\end{document}
