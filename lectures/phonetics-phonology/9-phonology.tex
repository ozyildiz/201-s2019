\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[margin=.3in]{geometry} 
\usepackage{../201-style}
\usepackage{mathtools}
\setitemize{itemsep=1pt,topsep=1pt,parsep=1pt,partopsep=1pt}
\setenumerate{itemsep=1pt,topsep=1pt,parsep=1pt,partopsep=1pt}
\usepackage{fancyhdr}
\pagestyle{fancy}
\renewcommand\headrulewidth{0pt}
\lhead{}\chead{}\rhead{}
\cfoot{\vspace*{-4em}\thepage}


\newcommand{\gap}{\underline{\phantom{xx}}}
\begin{document}
\setlength{\parindent}{0pt}
{\Large\bld{Linguistics 201 \quad Introduction to linguistic theory (spring 2019)}}\vspace{.25em}

{\large Deniz \"Ozy\i{}ld\i{}z \hfill \texttt{deniz.fr/201/}

Day 10 \tdot\  Underlying representations and phonological rules (February 26, 2019)
}\vspace{.75em}

\begin{itemize}
	\item Preliminaries
		\begin{itemize}
			\item \bld{Rule notation}: There's a useful shorthand that linguists use to write down phonological (and other) rules.  
				
				Phonological rules mostly involve changing a phoneme into one of its allophones, so rules specify:
				\begin{itemize}
					\item Something that changes

						/X/ {\color{gray} $\rightarrow$ [Y] / A\gap\ B}
					\item The change

						{\color{gray}/X/} $\rightarrow$ [Y] {\color{gray} / A\gap\ B}
					\item The environment in which the change occurs

						{\color{gray}/X/ $\rightarrow$ [Y]} / A\gap\ B 

				\end{itemize}
				How to read this: 
				\begin{itemize}
					\item ``/X/ is pronounced [Y] when /X/ comes after A and before B.''
					\item ``/X/ goes to [Y] between A and B.''
					\item It is implicit that /X/ is pronounced [X] elsewhere.
				\end{itemize}

				Examples: 
				\ex. \a. \tps{p} $\rightarrow$ \tpb{p\asp} / [$_{\text{syllable}}$ \gap \hfill (English)
				\b. \tps{l} $\rightarrow$ \tpb{\rng{l}} / voiceless consonant \gap \hfill (English) \\[-.5em]
				\b. \tps{r} $\rightarrow$ \tpb{\rng{r}} / \gap \# \hfill (Turkish, from Assignment \#2)
				\b. \tps{l} $\rightarrow$ \tpb{\rng{l}} / \gap \#\hfill (Turkish, from Assignment \#2)
				\z.  

				There is a technical thing (`features') that I haven't decided yet whether to introduce. It's not hard, but it's a lot of extra stuff. So, for now, we're also allowed to write things like:

				\ex. \a. voiceless stop $\rightarrow$ aspirated voiceless stop / [$_{\text{syllable}}$ \gap
				\b. approximant $\rightarrow$ voiceless approximant / voiceless consonant \gap
				\b. approximant $\rightarrow$ voiceless approximant / \gap \#
				\z.

				The way of writing rules like in \Last makes it clear that a given rule targets natural classes of sounds.

				(But it obscures the fact that \tps{p} goes to the corresponding \tpb{p\asp}, and not to any other aspirated voiceless stop.)

			\item \bld{Morphological analysis}: Words are (made up of) \bld{morphemes}.

				We'll talk about morphemes more starting next time. For today, you only need to know what they are.

				A morpheme is a smallest linguistic unit with a meaning.

				\begin{multicols}{2}
					\ex. \a. slow 
					\b. slow-ly
					\b. slow-er
					\z.

					\ex. \a. araba \hfill `car'
					\b. araba-lar \hfill `cars'
					\b. araba-lar-\i{} \hfill `their cars'
					\b. araba-lar-\i n{}-da \hfill `in their cars'

				\end{multicols}

		\end{itemize}
	\item \bld{Underlying representations}: 
		\begin{itemize}
			\item Phonemes come in different disguises, their allophones.
				\begin{itemize}
					\item Phonemes=mental representations of sounds
					\item Allophones=actual ways sounds get pronounced
				\end{itemize} 
			\item So far, the focus has been on individual phones and their environments. But, phones make up words:
				\begin{itemize} 
					\item Take the word `pat.' This word is made up of three phonemes: \tps{p}, \tps{\ae}, and \tps{t}. 
					\item But, there are at least three subtly different but mostly predictable ways of pronouncing this word.

						Focus on the \tps{t}.

						\ex. \tps{p\ae t}
						\a. \tpb{p\asp\ae P} \hfill (your tongue doesn't touch your alveolar ridge)
						\b. \tpb{p\asp\ae t\textcorner} \hfill (your tongue does touch your alveolar ridge and stays there)
						\b. \tpb{p\asp\ae t}\hfill (your tongue does touch your alveolar ridge and there's a release)\vspace{.5em}
						\b. \tpb{p\asp\ae d}\hfill (not a way of pronouncing `pat,' \tpb{d} is not an allophone of \tps{t} in English)
						\z.

				\end{itemize}
			\item How does an English speaker store the word `pat' in their mind?

				\begin{itemize}
					\item We make the hypothesis that the word `pat' is stored as the sequence of its phonemes: \tps{p\ae t}.  
						
						This is called an \bld{underlying representation} or a \bld{phonemic representation}.

						\begin{itemize}
							\item `Underlying,' because you never observe \tps{p\ae t} directly \& it corresponds to all of the actual pronunciations of the word.  
							\item `Phonemic,' because it is made up of a sequence of phonemes.

								\bld{Note}: Phonemes are enclosed in forward slashes \tps{lajk\dh Is}.
						\end{itemize}
					\item When we go about actually pronouncing this word, phonological rules have applied to it.

						\ex. \tps{p\ae t} \quad `pat'\hfill (underlying/phonemic form)
						\a. \tp{p\ae t} \hfill (syllabification)
						\b. \tp{p\asp\ae t} \hfill (aspiration for \tps{p}, obligatory)
						\b. \tp{p\asp\ae P} \hfill (glottalization for \tps{t}, optional)
						\z.
						\tpb{p\asp\ae P} \hfill (phonetic form)

						\ex. \tps{k2mpjut} \quad `compute'\hfill (underlying/phonemic form)
						\a. \tp{k2m.pjut} \hfill (syllabification)
						\b. \tp{k\asp 2m.p\asp jut} \hfill (aspiration for \tps{k} and \tps{p})
						\b. \tp{k\asp 2m.p\asp\rng{j}ut} \hfill (devoicing for \tps{j})
						\b. \tp{k\asp 2m.p\asp\rng{j}uP} \hfill (glottalization for \tps{t})
						\z.
						\tpb{k\asp 2m.p\asp\rng{j}uP} \hfill (phonetic form)

				\end{itemize}
		\end{itemize}
	\item Just like trying to figure out what the phonemes corresponding to allophones are, phonologists try to figure out words' underlying representations.

		In \Next, there is data from Russian.

		\begin{itemize}
			\item The `nominative form' of a noun is used when the noun is a subject: 
				\exg. Soldat goluboi \\
				soldier\gl{.nominative} is~light~blue \\
				\glt `The soldier is light blue.' 

			\item The `genitive form' of a noun is used when the noun is a possessor: 
				\exg. Drug soldat-a goluboi \\
				friend soldier\gl{-genitive} is~light~blue \\
				\glt `The soldier's friend is light blue.'

			\item The genitive form of a noun is formed by adding -a to the nominative form: soldat, soldata.
				\clearpage
		\end{itemize}
				\ex. Russian \\
				\begin{tabular}{llll}
					\toprule
					&Nominative singular & Genitive singular & Translation \\ 
					\midrule
					1 & vagon & vagona & `wagon'\\
					2& porok & poroga & `threshold'\\
					3&porok & poroka & `vice' \\
					4& \tp{muS} & \tp{muZa} & `husband'\\
					5& \tp{karandaS} & \tp{karandaSa} & `pencil' \\
					6& grip & griba & `mushroom' \\
					7& trup & trupa & `corpse' \\
					8& zavot & zavoda & `factory'\\
					9& soldat & soldata & `soldier'\\
					\bottomrule
				\end{tabular}

		\begin{itemize}
			\item The root of the noun remains the same in some forms: vagon, vagona; soldat soldata; etc. 
				
				In other forms, it changes: porok, poroga; mu\tp{S}, mu\tp{Z}a; grip, griba; etc.  

				(This is called an alternation.)
			\item The first question is: What are the underlying representations of the alternating forms?

				In other words, does a Russian speaker store the words\ldots
				\begin{itemize}
					\item for `threshold' as \tps{porok} or as \tps{porog}? \hfill (with a final \tps{k} or a final \tps{g}?)
					\item for `husband' as \tps{muS} or as \tps{muZ}?\hfill (with a final \tps{S} or a final \tps{Z}?)

					\item for `mushroom' as \tps{grip} or as \tps{griba}?\hfill (with a final \tps{p} or a final \tps{b}?)
					\item \ldots
				\end{itemize}
			\item The second question is: Why do these forms alternate?
				\begin{enumerate}
					\item Does Russian have a rule that says:\hfill (final devoicing rule)

						If I'm an oral consonant, devoice me at the end of a word!

						?
					\item Or, does Russian have a rule that says:\hfill (intervocalic voicing rule)

						If I'm an oral consonant, voice me between two vowels!

						?
				\end{enumerate}
			\item We have to try to answer both questions together.

				Look at the words for `threshold' and for `vice.'

				\begin{multicols}{2}
					\ex. \a. porok \quad `threshold' \\
					poroga \quad `of the threshold'
					\b. porok \quad `vice' \\
					poroka \quad `of the vice'

				\end{multicols}

				\begin{itemize}
					\item Suppose Russian had rule \#2, which says that oral consonants are voiced between two vowels.  

					 Then, we should never observe a voiceless consonant between two vowels. The word `poroka' should not be a word of Russian.
					\item But it is. It is pronounced `poroka' and it means `of the vice.'
					\item Suppose Russian has rule \#1, which says that oral consonants are devoiced at the end of words.
					\item Now suppose that the word for threshold has the underlying representation `porog,' with a \tpb{g}.
					\item Now suppose that the word for threshold has the underlying representation `porog,' with a \tpb{g}\ldots
						\begin{itemize}
							\item add an -a, you get `poroga,' which means `of the threshold.'
							\item apply the final devoicing rule, you get `porok,' which means `threshold.'
						\end{itemize}
				\end{itemize}
			\item Let's check that this makes the right kinds of predictions.

				\begin{itemize}
					\item Prediction \#1: Voiceless consonants are fine between vowels.

						They are: See 3, 5, 7, 9.
					\item Prediction \#2: Words do not end in voiced oral consonants.

						They don't: See 2 thru 9 in the first column.

					\item Prediction \#3: The only difference between the nominative and the genitive form of an alternating word is voicing.

						It is: See 2, 4, 6, 8.

						In other words, we don't get wild things like `poroga' for `of the threshold' and `porob' for `threshold.'
				\end{itemize}

				\begin{comment}
				\ex. Russian \\
				\begin{tabular}{llll}
					\toprule
					&Nominative singular & Genitive singular & Translation \\ 
					\midrule
					1 & vagon & vagona & `wagon'\\
					2& porok & poroga & `threshold'\\
					3&porok & poroka & `vice' \\
					4& \tp{muS} & \tp{muZa} & `husband'\\
					5& \tp{karandaS} & \tp{karandaSa} & `pencil' \\
					6& grip & griba & `mushroom' \\
					7& trup & trupa & `corpse' \\
					8& zavot & zavoda & `factory'\\
					9& soldat & soldata & `soldier'\\
					\bottomrule
				\end{tabular}
				\end{comment}

			\item \bld{Conclusion}: The underlying forms for 
				\begin{itemize}
					\item `wagon,' `vice,' `pencil,' `corpse,' `soldier,' are respectively `vagon,' `porok,' `karanda\tp{S},' `trup,' `soldat.'
					\item `threshold,' `husband,' `mushroom,' `factory,' are respectively `porog,' `mu\tp{Z},' `grib,' zavod.' 
				\end{itemize}
					The second set of forms have voiced consonants in their underlying representations. 
					
					When the voiced consonant ends up at the end of a word, it is devoiced.
		\end{itemize}

	\item English devoicing: The English plural is sometimes realized as \tpb{s}, sometimes as \tpb{z}. (For simplicity, we'll ignore \tpb{Iz}, as in `foxes' and we'll definitely ignore \tpb{en}, as in `oxen.')

		\begin{multicols}{2}
		\ex. English plurals\\
		\begin{tabular}{lll}
			\toprule
			\tpb{s} & \multicolumn{2}{c}{\tpb{z}} \\
			\midrule
			caps & cabs & clams \\
			cats & cads & cans \\
			picks & pigs & pings \\
			blimps & peas & blends \\
			\bottomrule
		\end{tabular}
		\columnbreak

			\begin{itemize}
				\item What is the underlying representation of the English plural suffix? Is it \tpb{s} or \tpb{z}?
				\item What are the two possible ways of analyzing the alternation between \tpb{s} and \tpb{z}?
			\end{itemize}
		\end{multicols}

		\begin{itemize}
			\item A devoicing rule would say:\\
				\tps{z} $\rightarrow$ \tpb{s} / voiceless consonant \gap
				\begin{multicols}{2}
				\ex. \a. \tps{k\ae pz}
				\b. \tpb{k\ae ps} \quad devoicing has applied
				\z.

				\ex. \a. \tps{k\ae bz}
				\b. \tpb{k\ae bz}
				\z.

				\end{multicols}

			\item A voicing rule would say:\\
				\tps{s} $\rightarrow$ \tpb{z} / voiced phone \gap
				\begin{multicols}{2}
				\ex. \a. \tps{k\ae ps}
				\b. \tpb{k\ae ps}
				\z.

				\ex. \a. \tps{k\ae bs}
				\b. \tpb{k\ae bz} \quad voicing has applied
				\z.

				\end{multicols} 
		\end{itemize}

		Both rules are equally simple and capture the data. How do we decide what the underlying representation of the English plural morpheme is, and whether English has a voicing rule or a devoicing rule? 

		We want our rules to be as general as possible. 
		\begin{itemize}
			\item Implicit in the voicing rule is that we should not expect to observe \tpb{s} after voiced sounds elsewhere in the language.

				This expectation is not borne out: `hiss,' `bass,' `false.'

			\item Implicit in the devoicing rule is that we should not expect to observe \tpb{z} after voiceless sounds elsewhere in the language.

				This expectation is borne out: There are no words like *\tpb{tIpz}, *\tpb{p\ae kz}.

			\item In fact, in general voiceless sounds can occur after voiced sounds: `path,' `danse,' `posh.'

				Voiced sounds do not occur after voiceless sounds: *\tpb{j\ae kd}, *\tpb{sdap}, *\tpb{pIsg}.

			\item[$\Rightarrow$] So, a devoicing rule fares better with additional and independent facts about English.

				Which strongly suggests that the plural morpheme in English is underlyingly \tps{z}.
		\end{itemize}

		\begin{comment}
	\item English vowel reduction

		\begin{tabular}{llll}
			\toprule
			\tp{'man@town} & monotone & \tp{m@'nat@nij} & monotony \\
			\tp{'tEl@gr\ae f} & telegraph & \tp{t@'lEgr@fij} & telegraphy \\
			\tp{'Ep@gr\ae f} & epigraph & \tp{@'pIgr@fij} & epigraphy \\
			\tp{'It@lij} & Italy & \tp{@'t\ae lj@n} & Italian \\
			\midrule
		\end{tabular}

	\item We saw that Russian has a final devoicing rule rather than an intervocalic voicing rule.

		This is not because intervocalic voicing rules do not exist.

		They do:

		\begin{multicols}{2}
		\ex. Italian
		\a. 

		For devoicing, what we have is words don't end in voiced consonants.

		For voicing, we would have there are no voiceless consonants between vowels.

		We need a pair map, mab and then neutralization intervocalically, so maba. If it were devoicing, we would not expect mab.
		\end{comment}


		\clearpage
	\item \bld{Kinds of phonological rules}
		\begin{itemize}
			\item \bld{Assimilation}: One sound becomes more like a neighboring sound.
				\ex. English nasal place assimilation:
				\a. impossible \tpb{Impasib\textsyllabic{l}}, indirect \tpb{Ind@\*rEkt}
				\b. unbelievable \tpb{2mb@liv@b\textsyllabic{l}}, unclear \tpb{2nklij\*r}
				\b. NPR \tpb{Empia\*r}
				\z. 

			\item \bld{Dissimilation}: One sound becomes less like a neighboring sound.
				\ex. Greek manner dissimilation: When one stop is followed by another, the first stop becomes a fricative
				\a. \tps{epta} $\rightarrow$ \tpb{efta} `seven'
				\b. \tps{ktizma}  $\rightarrow$ \tpb{xtizma} `building'

			\item \bld{Insertion}: Adds a sound
				\ex. \a. Turkish: \tpb{sWpor} `sport' 
				\b. Spanish: \tpb{estudjante} `student'
				\b. English: \tpb{d\ae nts} `dance'\quad \tpb{strE\ng kT} `strength' \quad \tpb{h\ae mpst\textsyllabic{r}} `hamster'

			\item \bld{Deletion}: Deletes a sound
				\ex. \a. `him' \tpb{hIm}
				\b. `Catch'im!' \tpb{k\ae tSIm}

			\item \bld{Strengthening}: Makes sounds `stronger.'\\
				English aspiration.
			\item \bld{Weakening}:  Makes sounds `weaker.'

				\ex. English flapping: Alveolar stops are realized as \tpb{R} after a stressed and before an unstressed vowel
				\a. `writer' \tpb{raIR\textsyllabic{\*r}}
				\b. `rider' \tpb{raIR\textsyllabic{\*r}}
				\z.

		\end{itemize}


\end{itemize}
\clearpage
		\includegraphics[trim={0cm 2.5cm 0cm 7.5cm},clip,scale=.75]{maltese1.jpg} 

		\includegraphics[trim={0cm 13cm 0cm 2.5cm},clip,scale=.75]{maltese2.jpg} 


\end{document}
