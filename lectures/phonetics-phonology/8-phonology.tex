\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[margin=.3in]{geometry} 
\usepackage{../201-style}
\usepackage{mathtools}
\setitemize{itemsep=1pt,topsep=1pt,parsep=1pt,partopsep=1pt}
\setenumerate{itemsep=1pt,topsep=1pt,parsep=1pt,partopsep=1pt}
\usepackage{fancyhdr}
\pagestyle{fancy}
\renewcommand\headrulewidth{0pt}
\lhead{}\chead{}\rhead{}
\cfoot{\vspace*{-4em}\thepage}


\newcommand{\gap}{\underline{\phantom{xx}}}
\begin{document}
\setlength{\parindent}{0pt}
{\Large\bld{Linguistics 201 \quad Introduction to linguistic theory (spring 2019)}}\vspace{.25em}

{\large Deniz \"Ozy\i{}ld\i{}z \hfill \texttt{deniz.fr/201/}

Day 9 \tdot\ Phonological rules (February 21, 2019) 
}\vspace{.75em}

\begin{itemize}
	\item The \bld{phonology} of a language includes:
		\begin{itemize}
			\item \bld{phonemes}, which are mental representations of sounds,
			\item \bld{rules}, which say which \bld{allophone} of a phoneme will get pronounced in different \bld{environments}.
		\end{itemize}

		% Here is our running example: English has a phoneme \tps{p}, which is pronounced as an aspirated stop \tpb{p\asp} when it occurs as the first syllable of an onset, and as an unaspirated stop \tpb{p} elsewhere. 

	\item Different languages have different phonemes, allophones, and rules.  
	\item Sometimes, a phoneme and its allophone look the same: \tpb{m} is an allophone of \tps{m}.

		Sometimes, the difference is subtle: \tpb{p\asp} is an allophone of \tps{p}.

		Sometimes, it is less so: \tpb{P} and \tpb{R} are allophones of \tps{t}.

		Sometimes, two phonemes have the same allophone: \tpb{R} is an allophone of \tps{t} (`writer') and of \tps{d} (`rider').

		$\Rightarrow$ You shouldn't be confused by this. The thing to keep in mind is that allophones are (mostly) predictable ways of pronouncing phonemes.

		\begin{comment}
		\begin{itemize}
			\item In English, aspirated voiceless stops and unaspirated voiceless stops are allophones of the same phoneme: A rule predicts when one is observed, versus the other. (See notes from previous lectures.) 
			\item In Hindi or Thai, aspirated voiceless stops and unaspirated voiceless stops are allophones of different phonemes: There is no rule to predict when one is observed, versus the other.

				\ex. \a. Thai\\
				\tpb{tam} `to pound' \quad \tpb{t\asp am} `to do' \hfill $\leftarrow$ minimal pair, distribution of \tpb{t} vs.\ \tpb{t\asp} unpredictable
				\b. Hindi\\
				\tpb{p@l} `moment' \quad \tpb{p\asp @l} `fruit' \hfill $\leftarrow$ minimal pair, distribution of \tpb{p} vs.\ \tpb{p\asp} unpredictable

		\end{itemize}
		\begin{itemize}
			\item Aspiration:

				In English, voiceless stops are aspirated when they are the first phone in a syllable onset, unaspirated otherwise. They are allophones of different phonemes.

				In Hindi and Thai, there is no rule to predict whether a voiceless stop is aspirated or not
		\end{itemize}

				\ex. \a. In English, the phonemes \tps{p}, \tps{t}, \tps{k} are produced as aspirated stops \tpb{p\asp}, \tpb{t\asp}, \tpb{k\asp} at the beginning of onsets, and as unaspirated stops \tpb{p}, \tpb{t}, \tpb{k} elsewhere.
				\b. In English, the phonemes \tps{\*r}, \tps{l}, \tps{w}, \tps{j} are produced as voiceless approximants \tpb{\rng{\*r}}, \tpb{\rng{l}}, \tpb{\rng{w}}, \tpb{\rng{j}} following a voiceless sound, and as voiced approximants \tpb{\*r}, \tpb{l}, \tpb{w}, \tpb{j} elsewhere.
				\b. In Turkish, the phonemes \tps{r} and \tps{l} are produced as voiceless approximants \tpb{\rng{r}} and \tpb{\rng{l}} at the end of a word, and as voiced approximants \tpb{r} and \tpb{l} elsewhere.

		\end{comment}
	\item The practical question is:

		Given two sounds [X] and [Y], how do we determine whether they're \bld{allophones of the same phoneme} or \bld{allophones of different phonemes}?

		\quad If [X] and [Y] are allophones of the same phoneme, they're ``two ways of producing the `same' sound.''

		\quad If [X] and [Y] are allphones of different phonemes, they're ``two ways of producing `different' sounds.''

		\begin{itemize}
			\item \bld{Task one}: Look for \bld{minimal pairs} where [X] and [Y] contrast.

			 In the following data sets, we compare short and long vowels. 
				
				Are there minimal pairs for short and long vowels?

				\begin{itemize}
					\item Kikuyu
						
						\ex. Vowel length in Kikuyu (Kenya, 6.6 million speakers) \\
						\begin{tabular}{llll}
							\toprule
							\multicolumn{2}{c}{short vowel} & \multicolumn{2}{c}{long vowel}\\
							\midrule
							\tpb{kera} & cross over & \tpb{ke:ra} & realize \\
							\tpb{\dh aka} & beautiful & \tpb{\dh a:ka} & play \\
							\tpb{kua} & die & \tpb{ku:a} & carry \\
							\tpb{\dh ura} & spit & \tpb{\dh u:ra} & stay \\
							\tpb{kOra} & find & \tpb{ko:ra} & little frog \\
							\bottomrule
						\end{tabular}

						\bld{There are minimal pairs contrasting short and long vowels in Kikuyu}. So we stop and conclude: In Kikuyu, \tpb{e} and \tpb{e:} are allophones of different phonemes. So are \tpb{a} and \tpb{a:}, \tpb{u} and \tpb{u:}, \tpb{O} and \tpb{O:}.  

					\item English

						\ex. Vowel length in English\\
						\begin{tabular}{llllll}
							\toprule
							\multicolumn{2}{c}{long vowels} & \multicolumn{4}{c}{short vowels} \\
							\midrule
							ride & \tpb{\*ra:jd} & right & \tpb{\*rajt} & rye & \tpb{raj} \\
							aid & \tpb{e:jd} & ate & \tpb{ejt} & bay & \tpb{bej} \\
							lobe & \tpb{lo:wb} & lope & \tpb{lowp} & low & \tpb{low} \\
							teethe & \tpb{t\asp i:\dh} & teeth & \tpb{t\asp iT} & tea & \tpb{ti} \\
							save & \tpb{se:jv} & safe & \tpb{sejf} & say & \tpb{sej} \\
							\bottomrule
						\end{tabular}

						\bld{There are no minimal pairs contrasting short and long vowels in English.}

				\end{itemize}
				If we don't find any minimal pairs, we go on to task two.
				\clearpage
			\item \bld{Task two}: Determine if there is a \bld{rule} that can derive [X] and [Y] from the same phoneme.

				To do this, we look at the \bld{environment} in which we find [X] and [Y]. The environment of a sound [X] is the sounds that come immediately before and after it.

				For our vowel length example, we want to see if we can find whether one of the following is true in English:
				\begin{itemize}
					\item A vowel shortening rule\\
						There's a rule that turns long vowels into short vowels in some environment.
					\item A vowel lengthening rule\\
						There's a rule that turns short vowels into long vowels in some environment.
				\end{itemize} 

				So we go through the data and write down the environments which the sounds of interest occur in:

		\end{itemize}

				\begin{multicols}{2}

			\ex.	Environments for long vs.\ short vowels\\
					\begin{tabular}{lll}
					\toprule
					long vowels & \multicolumn{2}{c}{short vowels} \\
					\midrule 
					\tpb{\*r\gap d} & \tpb{\*r\gap t} & [\tp{\*r\gap}\#]\\
					{}[\#\gap\tp{d}] & [\#\gap\tp{t}] & [\tp{b}\gap\#] \\
					{}[l\gap\tp{b}] & [l\gap\tp{p}] & [\tp{l}\gap\#] \\
					{}[\tp{t\asp}\gap\tp{\dh}] & [\tp{t\asp}\gap\tp{T}] & [\tp{t\asp}\gap\#] \\
					{}[s\gap\tp{v}] & [s\gap\tp{f}] & [\tp{s}\gap\#] \\
					\bottomrule 
			\end{tabular}\\[.5em] \# symbolizes the edge of a word. \\
			Pro tip: No sound has anything in common w/ \#.
					\columnbreak

						\ex. Vowel length in English\\
						\begin{tabular}{llllll}
							\toprule
							\multicolumn{2}{c}{long vowels} & \multicolumn{4}{c}{short vowels} \\
							\midrule
							ride & \tpb{\*ra:jd} & right & \tpb{\*rajt} & rye & \tpb{raj} \\
							aid & \tpb{e:jd} & ate & \tpb{ejt} & bay & \tpb{bej} \\
							lobe & \tpb{lo:wb} & lope & \tpb{lowp} & low & \tpb{low} \\
							teethe & \tpb{t\asp i:\dh} & teeth & \tpb{t\asp iT} & tea & \tpb{ti} \\
							save & \tpb{se:jv} & safe & \tpb{sejf} & say & \tpb{sej} \\
							\bottomrule
						\end{tabular}

				\end{multicols}\vspace{-2em}
				
				\begin{itemize}
					\item Looking at the environments for long vs.\ short vowels, we look for a pattern.
				\begin{itemize}
					\item Is there anything in common between the sounds that come before a long vowel?\\
						No: \tpb{\*r, l, t\asp, s} and \#
					\item Is there anything in common between the sounds that come after a long vowel?\\
						Yes: \tpb{d, b, \dh, v} are all \underline{\phantom{voiced consonants}}!
					\item Is there anything in common between the sounds that come before a short vowel?\\
						No: \tpb{\*r, l, t\asp, s} and \#
					\item Is there anything in common between the sounds that come after a long vowel?\\
						No: \tpb{t, p, T, f} and \#\\
						(Note here that if it weren't for \#, there would be something in common to these sounds, namely they would all be \underline{\phantom{voiceless consonants}}.)
				\end{itemize}
			If there is an environment unique to one allophone, we can write a rule that states that we will only see that allophone in that environment.

			\begin{itemize}
				\item If there's an environment where you find only short vowels, there's a rule that turns long vowels into short ones (a vowel shortening rule).
				\item If there's an environment where you find only long vowels, there's a rule that turns short vowels into long ones (a vowel lengthening rule).
			\end{itemize}

			What we found was that there's an environment in English where we find only long vowels and never short ones. We conclude that English has a vowel lengthening rule:

			\ex. The English vowel lengthening rule:\\
			In English, a short vowel is pronounced as a long vowel when it comes right before a voiced consonant.

			Given that we've found such a rule, we can also conclude that short vowels and long vowels in English are allophones of the same phoneme. 

			\begin{itemize}
				\item {}\tpb{a:j} and \tpb{aj} are allophones of the same phoneme \tps{aj}.
				\item {}\tpb{e:j} and \tpb{ej} are allophones of the same phoneme \tps{ej}.
				\item {}\tpb{o:w} and \tpb{ow} are allophones of the same phoneme \tps{ow}.
				\item {}\tpb{i:} and \tpb{i} are allophones of the same phoneme \tps{i}.
			\end{itemize} 
		\end{itemize}
	\item \bld{Natural classes}: 

		In task two above, we asked whether there was anything in common between certain sounds.

		Phonemes can be grouped together into \bld{natural classes}. 

		A natural class of sounds is a group of sounds that includes all and only sounds that share certain properties. 

		(These properties are also called \bld{features}.)

		The sounds that do not have those properties are not members of the natural class.

		\ex. Examples of natural classes
		\a. Voiceless (oral) stops: \tps{p}, \tps{t}, \tps{k}\\
		This set does not include \tps{b}, which is voiced, \tps{m}, which is nasal, \tps{f}, which is a fricative\ldots
		\b. Alveolar stops: \tps{t}, \tps{d}\\
		This set does not include \tps{p}, which is not alveolar, \tps{s}, which is not a stop\ldots
		\b. Nasals: \tps{m}, \tps{n}, \tps{\ng}
		\b. High tense vowels: \tps{i}, \tps{u}
		\z.

		\ex. Not examples of natural classes
		\a. \tps{p} and \tps{g}\\
		The thing that's common to \tps{p} and \tps{g} is that they're both oral stops. They differ in place of articulation and voicing. So the smallest natural class that includes \tps{p} and \tps{g} is the set of all oral stops.
		\b. \tps{u} and \tps{I}\\
		The thing that's common to \tps{u} and \tps{I} is that they're both high vowels. They differ in tense vs.\ lax, front vs.\ back, and rounding. The smallest naturall class that includes \tps{u} and \tps{I} is the set of high vowels: \tps{i}, \tps{I}, \tps{U}, \tps{u}.
		\b. \tps{p}, \tps{t}, \tps{k}, \tps{f}\\
		The thing that's common to these sounds is that they're all voiceless. But, there are other voiceless sounds: namely \tps{T}, \tps{S}, \tps{S}, \tps{tS} and \tps{dZ}.
		This will be the natural class of all and only voiceless sounds.
		\z.

		Phonological changes involve natural classes of sounds:

		\begin{itemize}
			\item Aspiration in English targets all and only voiceless oral stops.
				% In English, voiceless oral stops are aspirated in some environments, but voiced oral stops, nasals, etc., are never aspirated.
			\item In English, approximants are devoiced after voiceless sounds, but, e.g., nasals are not.\\
				\tpb{p\rng{\*r}ej}, \tpb{\*rej}; but \tpb{smal}, \tpb{mal} (not \tpb{s\rng{m}al}) 
			\item In English, vowels are nasalized when they come before nasals.\\
				`laboratory' \tpb{l\ae b\*r@toUri} \quad `language' \tpb{l\~\ae\ng gw@dZ}
			\item In Korean, \tps{s} is pronounced \tpb{S} when it comes right before high vowels.

				[See Korean exercise from next bullet point.]
		\end{itemize}

		When trying to solve phonology problems, it's good to keep in mind that such classes exist, and phonological rules  

	\item \bld{Phonological rules}: There's a shorthand that linguists use to write down phonological rules.

		\ex. \a. /X/ $\rightarrow$ [Y] / \gap\ B\\
		``/X/ is pronounced [Y] when it precedes B''
		\b. /X/ $\rightarrow$ [Y] / A\gap\\
				``/X/ is pronounced [Y] when it follows A''
		\b. /X/ $\rightarrow$ [Y] / A\gap\ B\\
				``/X/ is pronounced [Y] when it follows A and precedes B''

		Let's get some practice with this way of writing down phonological rules. In the Language Files exercise packet (Language Files 3.6), please find the exercises based on Korean, Burmese and Mokilese data.
		\begin{itemize}
			\item Korean illustrates the first kind of rule
			\item Burmese illustrates the second kind of rule
			\item Mokilese illustrates the third kind of rule
		\end{itemize} 
	
	\clearpage
	\item \bld{Kinds of phonological rules}
		\begin{itemize}
			\item \bld{Assimilation}: One sound becomes more like a neighboring sound.
				\ex. English nasal place assimilation:
				\a. impossible \tpb{Impasib\textsyllabic{l}}, indirect \tpb{Ind@\*rEkt}
				\b. unbelievable \tpb{2mb@liv@b\textsyllabic{l}}, unclear \tpb{2nklij\*r}
				\b. NPR \tpb{Empia\*r}
				\z. 

			\item \bld{Dissimilation}: One sound becomes less like a neighboring sound.
				\ex. Greek manner dissimilation: When one stop is followed by another, the first stop becomes a fricative
				\a. \tps{epta} $\rightarrow$ \tpb{efta} `seven'
				\b. \tps{ktizma}  $\rightarrow$ \tpb{xtizma} `building'

			\item \bld{Insertion}: Adds a sound
				\ex. \a. Turkish: \tpb{sWpor} `sport' 
				\b. Spanish: \tpb{estudjante} `student'
				\b. English: \tpb{d\ae nts} `dance'\quad \tpb{strE\ng kT} `strength' \quad \tpb{h\ae mpst\textsyllabic{r}} `hamster'

			\item \bld{Deletion}: Deletes a sound
				\ex. \a. `him' \tpb{hIm}
				\b. `Catch'im!' \tpb{k\ae tSIm}

			\item \bld{Strengthening}: Makes sounds `stronger.'\\
				English aspiration.
			\item \bld{Weakening}:  Makes sounds `weaker.'

				\ex. English flapping: Alveolar stops are realized as \tpb{R} after a stressed and before an unstressed vowel
				\a. `writer' \tpb{raIR\textsyllabic{\*r}}
				\b. `rider' \tpb{raIR\textsyllabic{\*r}}
				\z.

		\end{itemize}


\end{itemize}




\end{document}
