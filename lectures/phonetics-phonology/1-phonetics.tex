\documentclass[11pt]{article} 
\usepackage[margin=.5in]{geometry} 
\usepackage{201-style}
\setitemize{itemsep=0pt,topsep=0pt,parsep=0pt,partopsep=0pt}
\setenumerate{itemsep=0pt,topsep=0pt,parsep=0pt,partopsep=0pt}

\begin{document}
\pagestyle{empty}
\setlength{\parindent}{0pt}
{\Large\bld{Linguistics 201 \quad Introduction to linguistic theory (spring 2019)}}\vspace{.5em}

{\large Deniz \"Ozy\i{}ld\i{}z \hfill \texttt{deniz.fr/201/}

Day 2: Producing and representing speech sounds (January 24, 2019)
}\vspace{1em}

% Left-overs from last time?

% Pinker

\bld{Some facts}: 
\begin{itemize}[itemsep=0pt,topsep=0pt,parsep=0pt,partopsep=0pt]
	\item When we speak an oral (as opposed to signed) language, we produce speech sounds. 
	\item These sounds are produced with the lungs pushing air through our vocal folds and into the mouth and nose.
	\item Speech sounds must be mentally represented: We know which sounds to produce, when and how to produce them, we percieve them \emph{as} speech sounds rather than noise. (All of this, effortlessly in our native language.)
\end{itemize}

\bld{Some vocabulary}:
\begin{itemize}
	\item We will sometimes call the speech sounds of a language `phones.' % (This is just a word that comes from the Ancient Greek word for `sound,' it will become more handy later in the course.)
	\item ``Phonetics is concerned with describing the speech sounds of the languages of the world.'' (Ladefoged) 
		
		\begin{itemize}
			\item \emph{Articulatory} phonetics: How we produce (=articulate) speech sounds. 
			\item \emph{Acoustic} phonetics: What acoustic properties speech sounds have.
			\item \emph{Auditory} phonetics: How we percieve speech sounds.
		\end{itemize}
\end{itemize}

\bld{Fun activity}: Speech sounds are usually produced by \emph{pushing} air through the lungs. Is it possible to talk by \emph{inhaling} air, instead of exhaling it like usual?\\


If we're going to study the speech sounds of human languages, we need a way of representing them. Here, `representing' means being able to write them down, and to write about and talk about them. 

\begin{center}
	We want a way of representing \emph{every single speech sound in every single language}.
\end{center}

\bld{(Not a very good) idea}: The spelling of English words does give us an idea of how they're pronounced. So \emph{why not use English spelling} to represent the speech sounds of various languages?

In \emph{The Chaos}, Trenit\'e demonstrates the complexity of English spelling:\vspace{.3em}

\begin{minipage}[c]{.5\linewidth}
	\begin{quote}
		Dearest \emph{cr\bld{ea}ture} in \emph{cr\bld{e}ation}\\
		Studying English \emph{pronunc\bld{i}ation},\\
		\quad I will teach you in my verse\\
		\quad Sounds like \emph{corpse}, \emph{corps}, \emph{h\underline{o}rse} and \emph{w\underline{o}rse}.
	\end{quote}
\end{minipage}
\begin{minipage}[c]{.5\linewidth} 
	$\leftarrow$ The sound `ee' is written with the letters `ea,' `e,' `i,' etc.\\ 
	\\\\
	$\leftarrow$ The letter `o' is pronounced as in `horse,' or in `worse,' etc.

\end{minipage}\vspace{.3em}

\begin{itemize}
	\item \begin{itemize}
		\item How to pick the letters to use to represent the `ee' sound in, e.g., the Turkish word `b\bld{i}l\bld{i}yor' (meaning `they know')?  
		\item How to know which sound is meant by the letter `o' in the Turkish word `\underline{o}luyor' (meaning `it's happening')?
	\end{itemize} 
		Suppose we simply pick the letter `i' to represent the `ee' sound. This would also lead to the absurd result that we would now write `criture,' and `criation' instead of `creature' and `creation.' This is no longer English spelling.
	\item On top of all of this, there are many more speech sounds than there are letters in English. 
		\begin{itemize}
			\item Five letters for vowels: a, e, i, o, u
			\item Many more than five vowels: `beet, bit, bet, bat, b\underline{a}nh (as in banh mi), book, but, ball, bow\underline{e}l'
		\end{itemize} 

		And, there are many sounds that exist in languages that don't exist in English (Xhosa tongue twister video here).

		We could maybe use combinations of English letters to represent all of these sounds\ldots\ But, we won't.% it's easier to just use new symbols. % For example: b\tpb{i}t, b\tpb{I}t, b\tpb{E}, b\tpb{\ae}t, b\tpb{a}nh, b\tpb{U}k, b\tpb{\textsca}t, b\tpb{O}l, bow\tpb{@}l
		%\tp{i, I, E, \ae, A, U, \;A, @, O}
\end{itemize}

\bld{A quick exercise}: Because English spelling is not very transparent sometimes, the spelling of a word might be misleading as to the sounds that make it up. How many distinct sounds are there in the following words:  \emph{laugh}, \emph{begged}, \emph{graphic}, \emph{fish}, \emph{fishes}, \emph{fished}, \emph{batting}, \emph{quick}, \emph{these}, \emph{physics}, \emph{knock}, \emph{axis}.


\begin{center}
	The solution is to use \bld{The International Phonetic Alphabet} (IPA, for short).
\end{center}

\begin{itemize}
	\item The IPA is tried and true. It is also\ldots
		\begin{itemize}
			\item Unambiguous: It has \bld{one symbol per speech sound, and one speech sound per symbol}. 
			\item Universal: \bld{All known human speech sounds are represented}.
		\end{itemize}
	\item An important thing to note about IPA symbols:
		\begin{itemize}
			\item For some sounds, the IPA and English use the same symbol: [z] is a `z,' [p] is a `p,' [d] is a `d,' \ldots
			\item For some sounds, the IPA and English use different symbols: [j] is the `y' sound in `yo,' not the `j' sound in `Joe,' [i] is the `ee' sound in `beet,' not the `i' sound in `I,' etc.
			\item For some sounds, the IPA uses entirely different symbols from the ones found in the English alphabet: \tpb{T} is the `th' sound in `theater,' \tpb{\dh} is the `th' sound in `the,' etc.
		\end{itemize}
	\item An important convention: For now, we will always write IPA symbols in \bld{square brackets} to distinguish them from English letters \tpb{j} represents the `y' in `yo,' and `j' represents the `j' in `Joe.' 
	\item In order to go any further with the IPA, we need one last thing\ldots
		\begin{itemize}
			\item The IPA makes reference to the way sounds are produced by the lungs, the vocal tract, different parts of the mouth and the nose. For example \tpb{p} is a \emph{voiceless bilabial oral stop}. When you're making it, your vocal folds don't vibrate (voiceless), your mouth is fully obstructed (stop) at the lips (bilabial). No air flows through your nose when you produce it (oral).
			\item So to be able to decipher the IPA, we need to know more about the \bld{anatomy} of the lungs, the vocal tract, different parts of the mouth and the nose. 
			\item Why this makes sense: 
				\begin{itemize}
					\item Suppose I told you that \tpb{y} is the symbol for the vowel in the French word `lune.' This is not very useful unless you can produce this sound (in theory). And you can produce this sound if you `follow the instructions' for your tongue and lips.% Calling \tpb{y} \emph{high front rounded vowel}.
					\item Sounds that are made in similar ways and in similar places will display similar properties (more on this later). % The sounds \tpb{p, t, k} all have two things in common: The vocal folds don't vibrate, and you `obstruct' your mouth fully when making them. It turns out that they are the only sounds that come with a puff of air at the beginning of words 
				\end{itemize}
		\end{itemize}
\end{itemize}

% Suppose  Or [\tp{\oe}] is the symbol for the vowel in the Turkish word `\"ol.'

% But\ldots, remember how there are sounds that don't exist in English?  Perhaps this is manageable, but there are many bizarre speech sounds.

% So we need a way of characterizing these speech sounds, such that even if you never hear them in your life, or never know how to produce them yourself, you can still understand what they are and reason about them.

\begin{center}
	\bld{Enter \emph{articulatory phonetics}}
\end{center}

Languages have \bld{consonants} and \bld{vowels}. 

The main difference between them is that \bld{consonants} involve some constriction of airflow. 

We're going to describe \bld{consonants} based on 5 parameters:
\begin{itemize}
	\item \bld{Voicing}: Are the vocal folds vibrating during the production of the sound? E.g., \tpb{p} vs.\ \tpb{b} differ only in voicing.
	\item \bld{Place of articulation}: Where in the mouth is the constriction located? E.g., \tpb{p, t, k} differ in place.
	\item (\bld{Central} vs.\ \bld{lateral}: Is air flowing over the center of the tongue, or around the sides? The only lateral we'll see is \tpb{l}.)
	\item \bld{Nasality}: Is air flowing through your nose, or only through your mouth? E.g., the only difference between \tpb{d} and \tpb{n} is that the former is oral and the latter, nasal.
	\item \bld{Manner of articulation}: How constricted is the airflow? E.g., \tpb{d} involves full closure, \tpb{z} doesn't.
\end{itemize}\vspace{1em}

\bld{Vowels} involve an unobstructed airflow. They're described based on the position of your tongue. 
\begin{itemize}
	\item \bld{Frontness}: How close is the tongue to your front teeth? Try saying ``heed,'' which has a \bld{front} vowel vs.\ ``who'd,'' which has a \bld{back} vowel.
	\item \bld{Height}: How close is the tongue to the roof of the mouth? Try saying ``who'd,'' which has a \bld{high} vowel, and ``father,'' which has a \bld{low} first vowel.
	\item \bld{Tense} vs. \bld{lax}: Consider `beat,' (tense) and `bit' (lax); `boot' (tense) and `but' (lax).
	\item \bld{Lip rounding}: Are your lips rounded? The vowels in `boot' or in `book' are rounded, the ones in `father' and `beet,' are not.
\end{itemize}\vspace{1em}

The rest of this handout comes from \emph{A Course in Phonetics}.

\begin{minipage}[c]{.3\linewidth} 
	\includegraphics[width=\linewidth]{1-ladefoged.jpg}
\end{minipage}\hfill% 
\begin{minipage}[c]{.67\linewidth} 
%\includegraphics[height=10cm]{1-process.jpg}
	\bld{The oral and the nasal tracts (or cavities).}
	
	(Oral=mouth, nasal=nose, cavity=hole.)

	Loweing the velum lets air pass through your nose. 
	
	Raising the velum closes off the nasal cavity.

	Air is pushed by the lungs, through the vocal folds, into mouth or nose.

	(Vocal folds videos here.)
\end{minipage}


\begin{center}
	\bld{Places of articulation}
\end{center}
\begin{minipage}[c]{.37\linewidth}
	\includegraphics[width=\linewidth]{1-notthetongue.jpg}
\end{minipage}\hfill
\begin{minipage}[c]{.6\linewidth}
	\includegraphics[width=\linewidth]{1-places.jpg}
\end{minipage}

\begin{minipage}[c]{\linewidth}
	\begin{multicols}{2}
	\begin{enumerate}
		\item Bilabial: \tpb{p, b, m, w}
		\item Labiodental: \tpb{f, v}
		\item Dental: \tpb{T, \dh} (`movie \tpb{T}eater,' `here and \tpb{\dh}ere')
		\item Alveolar: \tpb{n, t, d, s, z, r, l}
		\item Palato-Alveolar: \tpb{S, Z} (`bed \tpb{S}eets,' `gara\tpb{Z}')
		\item Palatal: \tpb{j} (`master \tpb{j}oda')
		\item Velar: \tpb{\ng} (`opera sin\tpb{\ng}er'), \tpb{k, g, w}
		\item Glottal: \tpb{P} (`\tpb{P}uh-\tpb{P}oh!'), \tpb{h}

			(The glottis is the opening between the vocal folds.)
	\end{enumerate}
	\end{multicols} 
\end{minipage}


\begin{center}
	\bld{Manners of articulation}
\end{center}
\begin{minipage}[c]{\linewidth}
	\begin{multicols}{2}
		\begin{enumerate}
		\item Stop: 
			Complete closure of the mouth\\
			\tpb{p, t, k, P, b, d, g, m, n, \ng}
			\begin{itemize}
				\item Oral: No airflow through the nose\\ \tpb{p, t, k, P, b, d, g}
				\item Nasal: Yes airflow through the nose\\ \tpb{m, n, \ng}
			\end{itemize}
		\item Fricative: Partial obstruction of airflow, turbulence\\ \tpb{f, v, s, z, S, Z, h}
		\item Approximants: Partial obstruction of airflow, no turbulence\\ \tpb{j, w, l, r}
		\item Affricate: Stop+fricative \tpb{tS, dZ}
	\end{enumerate}
	\end{multicols}
\end{minipage}

\vfill
\bld{Some practice}

\includegraphics[scale=1]{1-ex1.jpg}

\includegraphics[scale=1]{1-ex2a.jpg}\vspace{-1em}

\includegraphics[scale=1]{1-ex2b.jpg}


\end{document}
There are symbols that you don't have to worry about: [p, b, t, d, k, g, f, v, s, z, h, m, n, l, r, w]

Disclaimer: In IPA, the English `r' is more accurately represented with an upside down `r,' [\tp{\*r}]. The IPA symbol [r] represents a rolled, or trilled, `r' like in the Spanish `arroz, burro, horror,' etc.

Some new consonant symbols: [\tp{P}, \tp{T}, \tp{\dh}, \tp{S}, \tp{Z}, \tp{tS}, \tp{dZ}, \tp{\ng}, j, \tp{\*w}]

\bld{A practical note} 

Exercises about the IPA will come in two (plus two) flavors.

Transcribe into IPA the following string\footnote{A `string' is a sequence of symbols, that is, of letters and spaces.} of English.

Write in English orthography the following string in IPA.

Provide the IPA symbol for X.

Provide the description for IPA symbol Y.
Every speech sound is produced by a different arrangement of the `articulators.'

(The articulators are the lips, the teeth, the tongue, and various places on the `roof' of the mouth.)

Let's take a couple of examples: The `p' sound in `pod,' and the `ee' sound in `beet.' How are they made?

In non-technical speak: 
\begin{itemize}
	\item The `p' sound in `pod,' is made by sealing the lips, building up pressure in the lungs and mouth, and releasing the lips.
	\item The `ee' sound in `beet' is produced by making your vocal folds vibrate while touching your lower front teeth with your tongue and your upper teeth with the sides of your tongue. 
	\item etc.  
\end{itemize}

In more technical speak:
\begin{itemize}
	\item The `p' sound in `pod' is a \emph{voiceless bilabial oral stop}: Your vocal folds don't vibrate, it's produced with your two lips, it's made in the mouth (not in the nose), and it involves full closure of your mouth.
	\item The `ee' sound in `beet' is a \emph{high front unrounded tense vowel}: Your tongue is raised and advanced in your mouth, your lips are not rounded (like they would be if you were saying `oh'), your tongue is `flexed,' and air can flow freely through your mouth.
\end{itemize}

What we have done is to describe technically how two sounds of English were produced by talking about what our mouth does when we are producing them.

$\Rightarrow$ We want to be able to this for every sound.

$\Rightarrow$ But, because we want to be able to make reference to each sound easily, we also need to learn a bunch of symbols that correspond to each sound.\footnote{We'll usually be dealing with the sounds of English, but we'll also deal with sounds that aren't found in English. They'll always be described in the necessary level of detail, and you won't have to memorize them for this class.}
\begin{itemize}
	\item The symbol [p] corresponds to the `p' sound in `pod.' It is a voiceless bilabial oral stop,
	\item The symbold [i] corresponds to the `ee' sound in `beet.' It is a high front unrounded tense vowel.
	\item etc.
\end{itemize}

Okay, we're talking about things like \emph{voiceless bilabial oral stops}\ldots\ but what exactly do these words mean?

Two of these words are very easy:
\begin{itemize}
	\item voiced vs.\ voiceless

		this is `voicing,' it's about whether your vocal folds vibrate when you produce a sound (voiced) vs.\ not (voiceless)

		a sound is voiced when your vocal folds vibrate, it is voiceless otherwise

	\item oral vs.\ nasal

		this is whether air travels through your nose or your mouth.   
\end{itemize}

The other two are harder, because there's some memorization involved  
\begin{itemize}
	\item \bld{place} of articulation: where the articulators are at
	\item \bld{manner} of articulation: how the articulators are where they're at
\end{itemize}

To figure that out, we need to look at some anatomy.
