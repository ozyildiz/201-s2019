\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[margin=.3in]{geometry} 
\usepackage{../201-style}
\usepackage{mathtools}
\usepackage{setspace}
\setitemize{itemsep=1pt,topsep=1pt,parsep=1pt,partopsep=1pt}
\setenumerate{itemsep=1pt,topsep=1pt,parsep=1pt,partopsep=1pt}
\usepackage{fancyhdr}
\pagestyle{fancy}
\renewcommand\headrulewidth{0pt}
\lhead{}\chead{}\rhead{}
\rfoot{\vspace*{-4em}\thepage}
\newcommand{\ruleme}{\underline{\hspace{\linewidth}}}

\newcommand{\ra}{$\rightarrow$\ }

\newcommand{\gap}{\underline{\phantom{xx}}}
\begin{document}
\setlength{\parindent}{0pt}
{\Large\bld{Linguistics 201 \quad Introduction to linguistic theory (spring 2019)}}\vspace{.25em}

{\large Day 15 \tdot\  Morphology (March 26th, 2019) \hfill Deniz \"Ozy\i{}ld\i{}z  \quad \texttt{deniz.fr/201/} 
}\vspace{.75em}

\begin{itemize}
	\item \bld{Previously, on morphology} 
		\begin{itemize}
			\item Words can be broken down into morphemes.
			\item There are various ways that words are created: prefixation, suffixation, infixation, compounding, \ldots
			\item We can represent the structure of a word by using trees.  
				\begin{itemize}
					\item Sometimes, to a given word correspond more than one structure: `unlockable, Alabama history teacher.'
					\item When this happens, the word often has more than one meaning.
				\end{itemize}

			\item For some words, their meaning and category are determined by the rightmost morpheme in the structure.

				E.g., an `Alabama history teacher' and `history teacher' are kinds of teachers, `AL history' is a kind of history.

				We called this the `Right Hand Head Rule.'

			\item Just like there are phonemes and allophones, there are morphemes and allomorphs.
				\begin{itemize}
					\item English has three ways of forming the (regular) past tense of verbs: \tpb{laj-d}, \tpb{l\ae f-t}, \tpb{fowld-@d} %\tpb{dag-z}, \tpb{k\ae t-s}, \tpb{faks-@z}.
					\item We can show that which plural suffix will show up is entirely predictable based on its phonological environment: \tpb{@d} after oral alveolar stops, \tpb{t} after voiceless consonants other than \tpb{t}, and \tpb{d} elsewhere.
					\item This suggests that the mental representation of the regular past tense suffix is \tps{d}. This is the morpheme.
					\item The various ways this morpheme actually gets realized are its allophones: \tpb{d}, \tpb{t} and \tpb{@d}.
				\end{itemize}
		\end{itemize}
	\item More on morphemes and allomorphs
		\begin{itemize}
			\item Additional examples of affix allomorphy

				The English regular plural suffix \tps{-z}, third person singular suffix \tps{-z}, and the past tense suffix \tps{-d} each have three allomorphs.

				\ex. \a. dogs, cats, foxes
				\b. runs, struts, uses
				\b. lied, laughed, folded
				\z.
				
			\item We hypothesize that these are allomorphs of the same morpheme for two main reasons:
				\begin{itemize}
					\item They mean the same thing,
					\item Which allomorph is used can be predicted by a general rule.
				\end{itemize}

			\item Note however that there are irregularities:

				\ex. \a. goose \ra geese, tooth \ra teeth, mouse \ra mice, louse \ra lice, \ldots
				\b. eat \ra ate, bite \ra bit, bike \ra boke (jk)

				\begin{itemize}
					\item The reason for these irregularities are often historical. 
					\item Crucially for us, however, we have to note that these forms cannot be predicted by the kinds of rules that we've been considering.

						\ex. Part of the English plural rule:\\
						To form the plural of a word, suffix \tpb{@z} if the word ends in a \tpb{s} or a \tpb{z} (a sibilant).

						So, the plural of `goose' is `geese,' The plural of `mouse' is `mouses,' etc.\quad No\ldots\ no\ldots\ no\ldots

					\item These irregularities have to be learned on a case by case basis. They are not predicted by our rules. 

						Note, however, that children acquiring English will say things like `gooses' or `goed,' instead of `geese' and `went.' This is really strong evidence that they have acquired a rule, but not the exceptions, and they are applying the rule to everything they know. 

						\clearpage

					\item So\ldots\ a better way of formulating our morphological rules goes like this:

						\ex. To form the plural of a word\ldots
						\a. Use the exceptional plural if there exists one,
						\b. Otherwise,
						\a. Suffix \tpb{@z} if the word ends in a sibilant,
						\b. Suffix \tpb{s} if the word ends in any other voiceless consonant,
						\b. Suffix \tpb{z} otherwise.


					\item Note here that we are going from the most specific statement to the least specific one.
					\item Note also that this way of formulating things makes it look like which allomorph is selected is `random.' 

						But often, this is not the case.

						\begin{itemize}
							\item We get \tpb{s} after voiceless consonants because sequences like \tpb{tz} are unpronounceable.
							\item We get \tpb{@z} after sibilants (presumably) because a) we want the plural to be perceptible, and b) adding a vowel between the root and the suffix will make it so.
						\end{itemize}
				\end{itemize}
			\item Roots have allomorphs too!

				\begin{itemize}
					\item Sometimes, the pronunciation of a root doesn't change when suffixes or prefixes are added.

						(Spelling might change while pronunciation doesn't.)

						\ex. \begin{tabular}{llll}
							\toprule
							bride & \tpb{brajd} & bridal & \tpb{brajd@l} \\
							profession & \tpb{pr@fES@n} & professional & \tpb{pr@fES@n@l}\\
							\bottomrule
						\end{tabular}

					\item Sometimes it does.

						(Though the spelling of the root might remain the same!)
						\ex. \begin{tabular}{llll}
							\toprule
							autumn & \tpb{aR@m} & autumnal & \tpb{aT@mn@l} \\
							hymn & \tpb{hIm} & hymnal & \tpb{hImnal} \\
							damn & \tpb{d\ae m} & damnation & \tpb{d\ae mnejS@n} \\
							condemn & \tpb{k@ndEm} & condemnation & \tpb{kand@mnejS@n} \\
							divide & \tpb{dIvajd} & divisible & \tpb{d@vIz@b@l} \\
							serene & \tpb{sErijn} & serenity & \tpb{s@rEnItij} \\
							recieve & \tpb{r@sijv} & receptive & \tpb{r@sEptIv} \\
							\bottomrule
						\end{tabular} 
					
					\item Where's the allomorphy?

						Take `damn' and `damnation.' `Damn' has no \tpb{n} in it. `Damnation' does!

						`Recieve' has an \tpb{ij} in it, and no \tpb{p}. `Receptive' has an \tpb{E} in it, and a \tpb{p}. (Compare also `reciept.')

						\ldots

					\item Again, some of these facts are due to historical reasons: recieve/receptive/reciept.

						Others are not necessarily so. For `damn,' it is likely that the \tpb{n} gets deleted because \tpb{mn} is not an acceptable coda in English. But when you add `-ation,' the \tpb{n} gets to be an onset. And the result is pronounceable.
				\end{itemize} 
		\end{itemize}
		\clearpage
	\item Identifying morphemes
		\begin{itemize}
			\item Consider the following data from Persian:
				\begin{multicols}{2}
					\begin{tabular}{@{}ll@{}}
						\toprule
						xaridam & I bought \\
						xaridi & you (sg.) bought \\
						xarid & they (sg.) bought\\
						naxaridam & I did not buy \\
						namixaridand & they (pl.) did not buy \\
						naxaridim & we did not buy \\
						mixarid & they (sg.) were buying \\
						\bottomrule 
					\end{tabular}\columnbreak

					 (\tpb{x} is just like \tpb{k}, but a fricative: a voiceless velar fricative; sg.=singular, pl.=plural)
					
				\end{multicols}

				\begin{itemize}
					\item Try to match each of the following meanings with a morpheme in the Persian data:
						\begin{multicols}{2}
						\ex. \a. I
							\b. you (sg.)
							\b. we
							\b. you (pl.)
							\b. they
							\b. not
							\b. was/were + ing
							\z.

						\end{multicols} 
					\item How would you say the following in Persian:
						\begin{multicols}{2}
							\ex. \a. They were buying
							\b. You (sg.) did not buy
							\b. You (sg.) were buying
							\z.

						\end{multicols}

					\item Here is a way of doing this:
						\begin{itemize}
							\item Identify the morpheme you're interested in, for instance the morpheme for `I.'
							\item Gather all the words that have a meaning that includes `I.' 

								We have `xaridam' and `naxaridam.'
							\item What do these words have in common? They seem to have an `-am' suffix. 
								
								This is likely the morpheme that we're interested in.
							\item 
						\end{itemize}
				\end{itemize}
			\item Consider the following data from Turkish:

				\ex. \begin{tabular}{llll}
					\toprule
					lokanta & restaurant & lokantada & at the restaurant \\
					randevu & appointment & randevuda & at the appointment \\
					kitap & book & kitapta & in the book \\
					taraf & side & tarafta & at the side \\
					el & hand & elde & in the hand \\
					s\tp{E}n & you & s\tp{E}nde & on you \\
					t\tp{S}ek & check & t\tp{S}ekte & on the check \\
					d\tp{Z}ep & pocket & d\tp{Z}epte & in the pocket \\
					\bottomrule
				\end{tabular}

				\begin{itemize}
					\item What are four ways of saying `in/at/on'?
					\item What are two things that differ across these four ways of saying `in/at/on'?
					\item What are the factors that seem to determine which way of saying `in/at/on' we will observe?
				\end{itemize}
		\end{itemize}
	 \item Infinity, and beyond

		 \ex. \a. The US have a missile.
		 \b. Russia came up with an anti missle.
		 \b. Well, the US invented an anti missle missle.
		 \b. Then Russia made an anti anti missle missle.
		 \b. Then the US was like, well, we have an anti anti missle missle missle
		 \b. tbc

\end{itemize}
\end{document}
