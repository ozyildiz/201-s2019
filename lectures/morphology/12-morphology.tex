\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[margin=.3in]{geometry} 
\usepackage{../201-style}
\usepackage{mathtools}
\usepackage{setspace}
\setitemize{itemsep=1pt,topsep=1pt,parsep=1pt,partopsep=1pt}
\setenumerate{itemsep=1pt,topsep=1pt,parsep=1pt,partopsep=1pt}
\usepackage{fancyhdr}
\pagestyle{fancy}
\renewcommand\headrulewidth{0pt}
\lhead{}\chead{}\rhead{}
\rfoot{\vspace*{-4em}\thepage}
\newcommand{\ruleme}{\underline{\hspace{\linewidth}}}

\newcommand{\ra}{$\rightarrow$\ }

\newcommand{\gap}{\underline{\phantom{xx}}}
\begin{document}
\setlength{\parindent}{0pt}
{\Large\bld{Linguistics 201 \quad Introduction to linguistic theory (spring 2019)}}\vspace{.25em}

{\large Day 13 \tdot\  Morphology (March 19th, 2019) \hfill Deniz \"Ozy\i{}ld\i{}z  \quad \texttt{deniz.fr/201/} 
}\vspace{.75em}

\begin{itemize}
	\item There are words.

		\begin{multicols}{3}
			\ex. \a. cat
			\b. wishbone
			\b. designer
			\b. undo
			\z.
			\columnbreak

		\end{multicols}
	\item Some words can't be broken further down into meaningful units: cat, table, yellow, catch\ldots

		Other words are made up of different meaningful units. 

		\begin{tabular}{llll}
			\toprule
			wishbone & wish & + & bone \\
			designer & design & + & -er \\
			undo & un- & + & do \\
			\bottomrule
		\end{tabular}

		\begin{comment}
			\begin{itemize}
				\item wishbone=wish+bone (two words)
				\item designer=design+er (the word `design' and the suffix `-er')
				\item undo=un+do (the word `do' and the prefix `un-')
			\end{itemize}
		\end{comment}

	\item \bld{Quick vocabulary time}
		\begin{itemize}
			\item \bld{Morpheme}: A sequence of sounds that has a meaning. E.g., `design, -er, un-, do.'
			\item \bld{\emph{Free} morpheme}: A morpheme that can stand on its own. E.g., `design, do.'
			\item \bld{\emph{Bound} morpheme}: A morpheme that can't stand on its own. E.g., `-er, un-.'
				\begin{itemize}
					\item `un-' is a prefix,
					\item `-er' is a suffix,
					\item Prefixes and suffixes attach to roots, like `design' and `do,'
					\item Prefixes and suffixes are called `affixes.'
				\end{itemize}
		\end{itemize}

	\item Morphemes have a \bld{meaning} associated with them.
		\begin{itemize}
			\item The meanings of roots `cat,' `wish,' and `bone,' are easy.
			\item The meanings of prefixes and suffixes are harder.
				\begin{itemize}
					\item What does `un-' mean?
					\item What does `-er' mean?
				\end{itemize}

			\item This is because their meaning depends on the meaning of the root.
				\begin{itemize}
					\item To `\bld{un}do' something is to reverse the action that did it.

						To `\bld{un}tangle' something is to reverse the action that tangled it.

						To `\bld{un}lock' something is to reverse the action that locked it.

						\ldots

						To `\bld{un}\emph{verb}' something is to reverse the action that \emph{verbed} it.

					\item A `design\bld{er}' is someone who designs.

						A `truck\bld{er}' is someone who trucks.

						A `sing\bld{er}' is someone who sings.

						\ldots

						A `\emph{verb}\bld{er}' is someone who \emph{verbs}.
				\end{itemize}
		\end{itemize}

	\item \bld{Quick exercise}: Please state how many morphemes there are in the following words, and whether those morphemes are free or bound.

		\begin{multicols}{3}
			\ex. \a. grapefruit
			\b. unlockable
			\b. nationalization
			\b. dehumanize
			\b. catalog
			\b. underestimated

		\end{multicols}

		\clearpage

	\item \bld{Morphological rules}: Words can be built from morphemes by using morphological rules.  

		\begin{itemize}
			\item But first, recall from school that words have \bld{categories} (``parts of speech'').

				\begin{itemize}
					\item \bld{Nouns}: cat, table, truck, pterodactyl, \ldots
					\item \bld{Verbs}: eat, catch, love, drive, \ldots
					\item \bld{Adjectives}: yellow, old, nice, sweet, \ldots 
				\end{itemize}

				(Don't be thrown off by the fact that the same word sometimes has different parts of speech: ``Trucks don't truck, truckers do.'' ``Use Google to google stuff.'')


			\item Now, let's work towards a rule for the English suffix `-er.'

				\begin{itemize}
					\item Observation \#1: The suffix `-er' attaches to verbs. It can't attach to other kinds of words.

						(The symbol `*' means that an expression is ungrammatical.) 

						\ex. \a. designer, trucker, singer, baker.
						\b. *pianoer, *chairer, *computerer
						\b. *funner, *tinyer, *marooner

					\item Observation \#2: When the suffix `-er' attaches to verbs, it creates a noun.

						\ex. \a. a designer: one who designs
						\b. a trucker: one who trucks
						\b. a singer: one who sings
						\b. a baker: one who bakes
						\z.

					\item The rule: In English, a noun can be created from a verb by attaching `-er' to the end of the verb.

						There is a formal notation for this rule:

						\ex. N $\rightarrow$ V + `-er'
						\a. N: a noun
						\b. $\rightarrow$: can be created from
						\b. V: a verb
						\b. +: combined with
						\b. `-er': the suffix `-er.'
						\z.

				\end{itemize}
			\item \bld{Quick exercise}: English has a suffix `-able,' as in, `washable' or `likeable.'
				\begin{itemize}
					\item What kinds of words does `-able' combine with, what kinds of words does it not combine with (out of nouns, adjectives and verbs)?
					\item What kinds of words does `-able' create when it attaches to a word?
					\item Based on these two observations, please write down a rule that says how to create words from other words by using the suffix `-able.'
				\end{itemize}

		\end{itemize}
	\item \bld{Homophony}: Sometimes, the same word has different meanings.  
		\begin{itemize}
			\item `orange' as in the color, or the fruit.
			\item `bank' as in the financial institution, or the side of a river.
		\end{itemize}

		The same observation applies to prefixes and suffixes.
		
		\begin{itemize}
			\item English has two suffixes `-er.' Here are the rules for both of these suffixes.
				\begin{itemize}
					\item N $\rightarrow$ V + `-er'
					\item A $\rightarrow$ A + `-er' 
				\end{itemize}
			\item English has two prefixes `un-.' Here are the rules for both of these suffixes.  
				\begin{itemize}
					\item V $\rightarrow$ `un-' + V 
					\item A $\rightarrow$ `un-' + A 
				\end{itemize} 
		\end{itemize}

		Can you come up with examples for each one of these morphemes?

		\clearpage
	\item More examples of morphemes, and morphological rules:
		\begin{itemize}
			\item The `-ness' rule
				\ex. \a. happiness, sadness, blueness, roundness
				\b. *walkness, *stealness, *screamness, *loveness
				\z.

				\ex. N $\rightarrow$ A + `-ness'\\[.5em]
				``The state of being happy/sad/blue/round/\ldots''

			\item The `-ing' rule
				\ex. \a. boring, singing, rolling, tiring
				\b. *hatting, *treeing, *hoteling, *billboarding
				\b. *blueing, *talling, *niceing
				\z.

				\ex. A $\rightarrow$ V + `-ing'

			\item The `re-' rule

				\ex. \a. redo, rewrite, reprint
				\b. *reblue, *retall, *renice
				\b. *rehat, *retree, *rehotel, *rebillboard

				\ex. V $\rightarrow$ `re-' + V 
				
		\end{itemize}

	\item Summary of our rules (there are many more)

		\begin{tabular}{ll}
			\toprule
			N \ra V + `-er' & player \\
			A \ra A + `-er' & taller \\
			A \ra V + `-able' & likeable \\
			N \ra A + `-ness' & happiness \\
			A \ra V + `-ing' & boring \\ 
			V \ra `un-' + V & undo \\
			A \ra `un-' + A & unpleasant \\
			V \ra `re-' + V & redo \\
			\bottomrule
		\end{tabular}\\

		Key intuition: Words are created from other words by using these rules.

		Even words that you might never have heard before!
		\begin{itemize}
			\item Suppose I tell you that ``Deniz \bld{torbled} yesterday at the talk,'' meaning that he asked really annoying and long questions to the speaker.
			\item Now you can say ``Deniz is a \bld{torbler}.''
		\end{itemize}

	\item Nothing prevents us from applying these rules multiple times.

		\begin{multicols}{2}
			\begin{tabular}{lll}
				\toprule
				& dress & \\
				V \ra `un-' + V & undress & \\
				V \ra `un-' + V & unundress & \\
				N \ra V + `-er' & unundresser & \\
				\bottomrule
			\end{tabular}

			\begin{tabular}{lll}
				\toprule
				& do & \\
				V \ra `un-' + V & undo & \\
				V \ra `re-' + V & reundo & \\
				N \ra V + `-er' & reundoable & \\
				\bottomrule
			\end{tabular} 
		\end{multicols}

	\item And these rules should prevent us from generating ungrammatical words.

		\begin{itemize}
			\item *retall is not a possible word because there is no rule that allows us to attach `re-' to an adjective (`tall').
			\item *playering is not a possible word because there is no rule that allows us to attach `-ing' to a noun (`player').
		\end{itemize}

		\clearpage

	\item Tree structures

		We would like a convenient way of representing how these words are created:
		\begin{itemize}
			\item Which morphemes are we putting together?
			\item In what order?
			\item What are their categories?
			\item What is the word and the category that results from putting them together?
		\end{itemize} 

		\begin{center}
			\bld{Tree structures} are a way of representing all of this information
		\end{center}

		\begin{multicols}{3}
			\begin{center}
				\begin{forest}
					[ N \\ player
						[ V \\ play ]
						[ -er ]
					] 
				\end{forest}
			\end{center}

			\begin{center}
				\begin{forest}
					[ A \\ doable
						[ V \\ do ]
						[ -able ]
					] 
				\end{forest}
			\end{center}

			\begin{center}
				\begin{forest}
					[ V \\ reprint
						[ re- ]
						[ V \\ print ]
					] 
				\end{forest}
			\end{center}
		\end{multicols}

	\item How to draw and read these?
		
		Take the tree for `player.' 
		
		\begin{itemize}
			\item We start with the root `play,' which is a verb. So we write down the root, and its category (V, for `Verb').

				\begin{center}
					\begin{forest}
						[
							[ V \\ play, edge={white} ]
							[ {\color{white}-er} , edge={white} ] 
						]
					\end{forest}
				\end{center}

			\item We will combine `play' with the suffix `-er,' by using the rule N \ra V + `-er.'

				So we write down the suffix to the right of the root. (To the left, if it's a prefix.)
				\begin{center}
					\begin{forest}
						[ 
							[ V \\ play , edge={white}]
							[ -er , edge={white}]
						]
					\end{forest}
				\end{center}

			\item We draw two meeting branches. This means that we're putting the two morphemes together.
				\begin{center}
					\begin{forest}
						[ 
							[ V \\ play ]
							[ -er ]
						]
					\end{forest}
				\end{center}

			\item The result of putting `play' and `-er' together and applying the `-er' rule creates the new word `player,' of category N (for `Noun'). We finally add these labels and we're done.

				\begin{center}
					\begin{forest}
						[ N \\ player
							[ V \\ play ]
							[ -er ]
						]
					\end{forest}
				\end{center}
		\end{itemize}

	\item Your turn: Please draw tree structures for `unpleasant' and for `undoer.' 

		\clearpage

	\item There are words that are ambiguous: They can mean two (or more) different things.

		\begin{multicols}{2}
			\ex. unlockable
			\a. something that can be unlocked
			\b. something that cannot be locked
			\z.

			\ex. unseeable
			\a. something that can be unseen
			\b. something that cannot be seen
			\z.

		\end{multicols}

		The reason that these words are ambiguous is that there are \bld{two ways of creating them}.

		\begin{itemize}
			\item Way \#1
				\begin{itemize}
					\item Start with the root `lock.'
					\item It's a verb, so we can apply the `un-' rule for verbs. We get `unlock.'
					\item `Unlock' is a verb, so we can apply the `-able' rule. We get `unlockable.'
					\item[$\Rightarrow$] Something that can be unlocked.

						\begin{forest}
							[ A \\ unlockable
								[ V \\ unlock
									[ un- ]
									[ V \\ lock ]
								] 
								[ -able ]
							]
						\end{forest}
				\end{itemize}
			\item Way \#2
				\begin{itemize}
					\item Start with the root `lock.'
					\item It's a verb, so we can apply the `-able' rule. We get `lockable.'
					\item `Lockable' is an adjective, so we can apply the `un-' rule for adjectives. We get `unlockable.'
					\item[$\Rightarrow$] Something that cannot be locked.

						Tree structure on your own:
						{\color{white}
						\begin{forest}
							[ A \\ unlockable
								[ V \\ unlock
									[ un- ]
									[ V \\ lock ]
								] 
								[ -able ]
							]
						\end{forest} 
						}
			\end{itemize}
		\end{itemize} 

	\item
\end{itemize}

\end{document}
