\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[margin=.3in]{geometry} 
\usepackage{../201-style}
\usepackage{mathtools}
\usepackage{setspace}
\setitemize{itemsep=1pt,topsep=1pt,parsep=1pt,partopsep=1pt}
\setenumerate{itemsep=1pt,topsep=1pt,parsep=1pt,partopsep=1pt}
\usepackage{fancyhdr}
\pagestyle{fancy}
\renewcommand\headrulewidth{0pt}
\lhead{}\chead{}\rhead{}
\rfoot{\vspace*{-4em}\thepage}
\newcommand{\ruleme}{\underline{\hspace{\linewidth}}}

\newcommand{\ra}{$\rightarrow$\ }

\newcommand{\gap}{\underline{\phantom{xx}}}
\begin{document}
\setlength{\parindent}{0pt}
{\Large\bld{Linguistics 201 \quad Introduction to linguistic theory (spring 2019)}}\vspace{.25em}

{\large Day 14 \tdot\  Morphology (March 21st, 2019) \hfill Deniz \"Ozy\i{}ld\i{}z  \quad \texttt{deniz.fr/201/} 
}\vspace{.75em}

\begin{itemize}
	\item Other kinds of affixes and ways of forming new words
		\begin{itemize}
			\item Infixes\hfill [a morpheme goes \emph{inside} a word]
				\ex. Chamorro (Austronesian, Mariana Islands)\\
				The infix `-um-' combines with adjectives and means `to become' that adjective.\\
				It is inserted right after the first onset in the word.\\
				\begin{tabular}{llll}
					\toprule
					\tpb{dankolo} & `big' & \tpb{d\underline{um}ankolo} & `become big'\\
					\tpb{t\*risti} & `sad' & \tpb{t\*r\underline{um}isti} & `become sad'\\
					\bottomrule
				\end{tabular}

			\item Circumfixes\hfill [a morpheme goes \emph{around} a word] 

				\ex. Axininca Campa (Arawakan Peru) \hfill (from Assignment 3)\\
				The circumfix `no\ldots ti' means `my'\\
				\begin{tabular}{llll}
					\toprule 
					toniro & `palm' & notoniroti & `my palm' \\
					jaarato & `black bee' & nojaaraatoti & `my black bee' \\
					\bottomrule
				\end{tabular}

				\ex. English `en\ldots en'
				\a. light $\rightarrow$ enlighten
				\b. live $\rightarrow$ enliven
				\b. bold $\rightarrow$ embolden
				\b. large $\rightarrow$ enlarge
				\z.

			\item Reduplication
				\ex. Turkish\\
				Part of an adjective is copied and tacked on to the beginning of the adjective, with the meaning `very.'
				\begin{tabular}{llll}
					\toprule
					mavi & `blue' & masmavi & `very blue' \\
					kod\tp{Z}a & `huge' & koskod\tp{Z}a & `very huge' \\
					ba\tp{S}ka & `different' & bamba\tp{S}ka & `very different \\
					\bottomrule
				\end{tabular}

				\ex. English 
				\a. Was it a date? Or was it a \emph{date date}?
				\b. Do you take a glass of wine every now and then? Or do you like \emph{drink drink}?
				\z.

		\end{itemize}
	\item \bld{Quick exercise} on English `blooming' infixation
		
		English has a rule of `blooming' infixation:

		\ex. \a. Alabama $\rightarrow$ Ala-bloomin-bama
		\b. *A-bloomin-labama
		\b. *Alaba-bloomin-ma
		\z.

		Based on the data below, please think about what that rule might be. Where in a word does `blooming' like to sit? (Hint: The acute accent \'{} marks the position of the word's primary stress.)

		\ex. More English `blooming' infixation data\\
		\begin{tabular}{lll}
			\toprule
			incr\'edible & in-bloomin-credible & *incredi-bloomin-ble \\
			fant\'astic & fan-bloomin-tastic & *fanta-bloomin-stick \\
			Mass\'achusetts & Massa-bloomin-chusetts & *Massachu-bloomin-sets \\
			Calif\'ornia & Cali-bloomin-fornia & *Californi-bloomin-ia \\
			\bottomrule
		\end{tabular}

		\clearpage
	\item Compounding

		\begin{itemize}
			\item Up `til now, we have seen ways of making new words by taking a root and attaching affixes to it.

				It's also possible to build words up by compounding two or more words.

				\ex. \a. V \ra N + V\\ 
				bodybuild, birdwatch, housepaint
				\b. V \ra A + V\\
				finetune, doublebook, broadcast
				\b. N \ra A + N\\
				blackboard, hotdog, schoolteacher
				\b. N \ra V + N\\
				swearword, rattlesnake, sellsword
				
			\item A generalization emerges: 

				The \bld{category} of the compound is the category of the rightmost word that makes up the compound.

				\ex. \a. `build' is a verb, so is `bodybuild' 
				\b. `tune' is a verb, so is `finetune'
				\b. `board' is a noun, so is `blackboard'
				\b. `word' is a noun, so is `swearword'

				\begin{multicols}{4}
					\begin{forest}
						[ V \\ bodybuild
						[ N \\ body ]
						[ V \\build ]
						]
					\end{forest}

					\begin{forest}
						[ V \\ finetune
						[ A \\ fine ]
						[ V \\ tune ]
						]
					\end{forest}

					\begin{forest}
						[ N \\ blackboard
						[ A \\ black ]
						[ N \\ board ]
						]
					\end{forest}

					\begin{forest}
						[ N \\ swearword
						[ V \\ swear ]
						[ N \\ word ]
						]
					\end{forest}
				\end{multicols} 

				The \bld{meaning} of the compound is also (usually) determined by the meaning of the rightmost word in the compound.  

				\ex. \a. A `wishbone' is a kind of bone
				\b. A `bonewish' is a kind of wish
				\b. To `birdwatch' is a kind of watching
				\b. To `broadcast' is a kind of casting
				\z.

				{\small \bld{fine print}: Note that this is not always the case: A `cutthroat' is not a kind of throat, a `breakwater' is not a kind of water, a `breakfast' isn't a kind of fast, etc.}


				The expression that determines the category and the meaning of a larger expression is called a \bld{head}.

				\begin{itemize}
					\item The head of the verb `bodybuild' is the verb `build.'
					\item The head of the noun `blackboard' is the noun `board'
				\end{itemize} 

				\begin{mdframed}
				\ex. \bld{The Right Hand Head Rule}: In a word that is made up of two morphemes X and Y, the category and the meaning of the word is determined by the rightmost morpheme.\\ 
					\begin{center}
					\begin{forest}
						[ X 
						[ Y ]
						[ X  ]
						]
					\end{forest}
					\end{center}

				\end{mdframed}

		\end{itemize} 
		\clearpage 
	
	\item Compounds can be longer than two words. Here is one. Note that it is ambiguous:

		\ex. Alabama history teacher
		\a. A history teacher who works in Alabama
		\b. A teacher of Alabama history
		\z.

		The reason for this ambiguity is that the string of words `Alabama history teacher' has to two different structures.

		\begin{multicols}{2}
			\begin{center}
				A history teacher working in Alabama:

				\begin{forest}
					[ N \\ Alabama history teacher 
						[ N \\ Alabama ]
						[ N \\ history teacher
							[ N \\ history ]
							[ N \\ teacher ]
						]
					]
				\end{forest}

				A teacher of Alabama history:

				\begin{forest}
					[ N \\ Alabama history teacher 
						[ N \\ Alabama history 
							[ N \\ Alabama ]
							[ N \\ history ]
						]
						[ N \\ teacher ] 
					]
				\end{forest}
			\end{center}
		\end{multicols}

	\item Please take a moment to reflect upon the fact that these compounds obey the Right Hand Head Rule at each combination `step.'
	\item How about these ones:
		\ex. Hershey bar protest
		\a. 
		\b. 
		\z.

		\ex. cuckoo bird watch
		\a. 
		\b.
		\z.

	\item Quick return to prefixes and suffixes

		\begin{itemize}
			\item Last time, I was hedgy on the category and the meaning contribution of prefixes and suffixes.

			Let's fix that.

			\begin{multicols}{3}
				\begin{center}
					\begin{forest}
						[ N \\ singer 
							[ V \\ sing ]
							[ ?? \\ -er ]
						]
					\end{forest}

					\begin{forest}
						[ V \\ undo 
							[ ?? \\ un- ]
							[ V \\ do ]
						]
					\end{forest}

					\begin{forest}
						[ V \\ sings 
							[ V \\ sing ]
							[ ?? \\ -s ]
						]
					\end{forest}
				\end{center}

			\end{multicols}

		\item One thing to observe is that \bld{some affixes change the category of the word they attach to}, e.g., `-er.'


				The \bld{Right Hand Head Rule} makes a prediction as to what category this set of affixes should belong to. 

				%\newcommand{\gap}{\underline{\phantom{XX}}}
				\begin{multicols}{3}
					\begin{center}
						\begin{forest}
							[ N \\ singer 
								[ V \\ sing ]
								[ \gap \\ -er ]
							]
						\end{forest}

						\begin{forest}
							[ A \\ singable 
								[ V \\ sing ]
								[ \gap \\ -able ]
							]
						\end{forest}

						\begin{forest}
							[ N \\ happiness 
								[ A \\ happy ]
								[ \gap \\ -ness ]
							]
						\end{forest}

						\begin{comment}
						\begin{forest}
							[ A \\ boring 
								[ V \\ bore ]
								[ \gap \\ -ing ]
							]
						\end{forest}
						\end{comment}
					\end{center}
				\end{multicols}

			\clearpage

		\item The Right Hand Head Rule makes no prediction as to what the categories of prefixes are.

			This is because the rule is about the rightmost morpheme in a word, and prefixes are never rightmost.

			\begin{multicols}{3}
				\begin{center}
					\begin{forest}
						[ V \\ undo 
							[ ?? \\ un- ]
							[ V \\ do ]
						]
					\end{forest}

					\begin{forest}
						[ A \\ unhappy 
							[ ?? \\ un- ]
							[ A \\ happy ]
						]
					\end{forest}

					\begin{forest}
						[ V \\ redo 
							[ ?? \\ re- ]
							[ V \\ do ]
						]
					\end{forest} 
				\end{center} 
			\end{multicols}
			
		\item For suffixes that don't change the category of the word they attach to, things seem to be straightforward.

			We could assume that they are of the same category as the word they attach to.

			In either case the Right Hand Head Rule would make the right prediction.

			\begin{multicols}{3}
				\begin{center}
					\begin{forest}
						[ V \\ walked 
							[ V \\ walk ]
							[ V? \\ -ed ]
						]
					\end{forest}

					\begin{forest}
						[ V \\ walks 
							[ V \\ walk ]
							[ V? \\ -s ]
						]
					\end{forest}

					\begin{forest}
						[ N \\ dogs 
							[ N \\ dog ]
							[ N? \\ -s ]
						]
					\end{forest}
				\end{center}
			\end{multicols}
			
		\item We could just as well assume that these prefixes and suffixes don't have any category:

			\begin{multicols}{2}
				\begin{center}
					\begin{forest}
						[ V \\ undo 
							[ un- ]
							[ V \\ do ]
						]
					\end{forest}

					\begin{forest}
						[ V \\ walks 
							[ V \\ walk ]
							[ -s ]
						]
					\end{forest}
				\end{center}
			\end{multicols}

		\item At this stage, observe that some affixes change the meaning of the base word `more drastically' than others.

			The intuition to check here is, are these pairs of words the same word, or two different words?
				\ex. \a. \a. sing, singer
				\b. sing, singable,
				\b. happy, happiness \hfill $\leftarrow$ category change and meaning change\vspace{.5em}
				\z.
				\b. \a. sing, sings
				\b. laugh, laughed
				\b. dog, dogs\hfill $\leftarrow$ no category change, and meaning preserved\vspace{.5em}
				\z. 
				\b. \a. do, undo
				\b. do, redo
				\b. happy, unhappy \hfill $\leftarrow$ no category change, but meaning change
				\z.

				\begin{itemize}
					\item If you say `I sing' vs.\ `Mary sings,' you've changed the verb from `sing' to `sings.' But the meaning of the word basically remains constant.  
					\item Compare this to saying ``I'm happy'' vs.\ ``I'm unhappy.'' Totally different meanings.
				\end{itemize}

			\item \bld{Quick vocabulary time}
				\begin{itemize}
					\item The kind of affix that usually changes the category and the meaning of a word is called a \bld{derivational} morpheme. 
					\item The kind of affix that usually doesn't change the category or the meaning of a word is an \bld{inflectional} morpheme.
				\end{itemize}
		\end{itemize} 

		\clearpage
	\item If time: \bld{Morpho-phonological analysis}

		\begin{tabular}{ccc}
			\toprule 
			past tense in \tpb{d} & past tense in \tpb{t} & past tense in \tpb{@d} \\
			\midrule
			buzz-\tpb{d}& bake-\tpb{t}  & fold-\tpb{@d} \\
			sigh-\tpb{d}& stuff-\tpb{t} & bat-\tpb{@d} \\
			grab-\tpb{d}& jump-\tpb{t}  & lift-\tpb{@d} \\
			call-\tpb{d}& kiss-\tpb{t}  & seed-\tpb{@d} \\
			fan-\tpb{d} & wish-\tpb{t} & \\
			save-\tpb{d}& froth-\tpb{t} &\\ 
			boo-\tpb{d}&  &\\ 
			\bottomrule
		\end{tabular}\\

		\begin{itemize}
			\item English seems to have three morphemes to express past tense on a verb. Although this morpheme is written `-ed,' it is sometimes pronounced \tpb{-d}, sometimes \tpb{-t}, sometimes \tpb{-@d}.
			\item Does an English speaker need to memorize each verb form with the past tense form that it goes with? Or can they predict which form to use by rule?\hfill (You should be noticing a pattern here\ldots)
			\item One reason to think that there is a general rule comes from the fact that we can form the past tense of verbs that we've never heard before:
				\ex. \a. \tpb{sl2b} \ra \tpb{sl2bd}
				\b. \tpb{sl2k} \ra \tpb{sl2kt}
				\b. \tpb{sl2d} \ra \tpb{sl2d@t}

			\item What are the environments in which we observe \tpb{d}, \tpb{t} and \tpb{@d}? 

				\ex. \a. List of phones that precede \tpb{d}:
				\b. List of phones that precedde \tpb{t}:
				\b. List of phones that precede \tpb{@d}:
				\z.

				Are there things in common within these three lists?

			\item Just like there are phonemes and allophones, there are morphemes and allomorphs.

			\item Assume that \tps{-d} is the underlying representation of the English past tense morpheme. This morpheme has three allomorphs that are fully predictable in their distribution.

				What are the two rules that predict the distribution of \tps{-d}'s allomorphs?

		\end{itemize}
\end{itemize}

\end{document}
